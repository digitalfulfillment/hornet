export const environment = {
    name: 'Hornet',
    production: false,
    apiUrl: 'http://localhost',
    passwordGrant: {
        client_id: null,
        client_secret: null
    },
    pusher: {
        key: null,
        cluster: null,
    }
}