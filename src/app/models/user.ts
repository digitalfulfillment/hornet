import { Role } from './role';

export class User {
    id : number;
    name: string;
    email: string;
    is_admin: boolean;
    created_at: Date;
    updated_at: Date;
    roles?: Role[];

    constructor(options: {
        id?: number,
        name: string,
        email: string,
        created_at?: string,
        updated_at?: string,
    }) { 
        this.id = options.id;
        this.name = options.name;
        this.email = options.email;
        this.created_at = new Date(options.created_at);
        this.updated_at = new Date(options.updated_at);
    }
}
