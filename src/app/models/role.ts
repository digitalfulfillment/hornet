export class Role {
    id: number;
    name: string;
    description: string;
    created_at: Date;
    updated_at: Date;

    constructor(options: {
        id?: number,
        name?: string;
        description?: string;
        created_at?: Date;
        updated_at?: Date;
    } = {}) {
        this.id = options.id;
        this.name = options.name;
        this.description = options.description;
        this.created_at = new Date(options.created_at);
        this.updated_at = new Date(options.updated_at);
    }
}
