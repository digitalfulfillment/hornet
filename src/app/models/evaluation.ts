import { CallDetail } from './call-detail';
import { Category } from './category';
import { Agent } from './agent';
import { Campaign } from './campaign';
import { Team } from './team';
import { WorkQueue } from './work-queue';
import { User } from './user';

export class Evaluation {
    id: number;
    agent_id: number;
    agent: Agent;
    campaign_id: number;
    team_id: number;
    team: Team;
    work_queue_id: number;
    work_queue: WorkQueue;
    user_id: number;
    reference_code: string;
    auto_fail: boolean;
    user: User;
    call_history: string;
    call_details: CallDetail[];
    content: Category[];
    remarks: string;
    score: number;
    total_points: number;
    total_items: number;
    created_at: Date;
    updated_at: Date;

    constructor(options: {
        id?: number;
        agent_id: number;
        campaign_id: number;
        team_id: number;
        work_queue_id: number;
        reference_code: string;
        auto_fail: boolean;
        call_history: string;
        call_details?: CallDetail[];
        content: Category[];
        remarks?: string,
        score?: number;
        total_points?: number;
        total_items?: number;
        created_at?: Date;
        updated_at?: Date;
    }) {
        this.id = options.id;
        this.agent_id = options.agent_id;
        this.campaign_id = options.campaign_id;
        this.team_id = options.team_id;
        this.work_queue_id = options.work_queue_id;
        this.reference_code = options.reference_code;
        this.auto_fail = options.auto_fail;
        this.call_history = options.call_history;
        this.call_details = options.call_details;
        this.content = options.content;
        this.remarks = options.remarks;
        this.score = options.score;
        this.created_at = options.created_at;
        this.updated_at = options.updated_at;
    }
}
