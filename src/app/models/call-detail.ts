export class CallDetail {
    constructor(
        public label: string,
        public description: string
    ) { }
}
