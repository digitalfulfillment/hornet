import { Category } from './category';

export class Questionnaire {
    id: number;
    name: string;
    content: Category[]; 
    created_at: Date;
    updated_at: Date;

    constructor(options: {
        id?: number,
        name: string,
        content?: Category[],
        created_at?: string,
        updated_at?: string,
        deleted_at?: string,
    }) {
        this.id = options.id;
        this.name = options.name;
        this.content = options.content;
        this.created_at = new Date(options.created_at);
        this.updated_at = new Date(options.updated_at);
    }
}
