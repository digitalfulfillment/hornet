import { Question } from './question';

export class Category {
    name: string;
    questions: Question[];

    constructor(options: {
        name: string,
        questions: Question[]
    }) {
        this.name = options.name,
        this.questions = options.questions
    }
}
