export class Campaign {
    id: number;
    name: string;
    created_at: Date;
    updated_at: Date;
    deleted_at?: Date;

    constructor(options: {
        id?: number,
        name: string,
        created_at?: string,
        updated_at?: string,
        deleted_at?: string
    }) {
        this.id = options.id;
        this.name = options.name;
        this.created_at = new Date(options.created_at);
        this.updated_at = new Date(options.updated_at);
        this.created_at = new Date(options.created_at);
    }
}
