export class Question {
    applicable?: boolean;
    points: number;
    question: string;
    remarks?: string;
    score?: number;

    constructor(options: {
        applicable?: boolean,
        points: number,
        question: string,
        remarks?: string
        score?: number,
    }) {
        this.applicable = options.applicable;
        this.points = options.points;
        this.question = options.question;
        this.remarks = options.remarks;
        this.score = options.score;
    }
}
