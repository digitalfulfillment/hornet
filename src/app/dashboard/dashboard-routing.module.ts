import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkQueueResolverService } from './work-queue-resolver.service';
import { DashboardComponent } from './dashboard.component';
import { WorkQueueDataComponent } from './work-queue-data/work-queue-data.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { 
    path: 'campaign/:campaignId/work-queue/:workQueueId/month/:month/year/:year',
    component: WorkQueueDataComponent,
    resolve: {
      data: WorkQueueResolverService,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [WorkQueueResolverService]
})
export class DashboardRoutingModule { }
