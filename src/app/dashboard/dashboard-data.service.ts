import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '../core/api.service';
import { Campaign } from '../models/campaign';
import { WorkQueue } from '../models/work-queue';
import { Paginated } from '../interfaces/paginated';

const url = 'dashboard';

@Injectable()
export class DashboardDataService {

  constructor(private api: ApiService) { }

  index(query: { month: string, year:number }): Observable<Campaign[]> {
    return this.api.post(`${url}`, query);
  }

  refresh(query: { month: string, year:number }): Observable<Campaign[]> {
    return this.api.post(`${url}/refresh`, query);
  }

  workQueue(query: { campaign_id, work_queue_id, month, year }): Observable<WorkQueue[]> {
    return this.api.post(`${url}/work-queue`, query);
  }

  formatDates(dateRanges) {
    return dateRanges.map(dateRange => `${new Date(dateRange.from.date).toDateString()} to ${new Date(dateRange.to.date).toDateString()}`);
  }
}
