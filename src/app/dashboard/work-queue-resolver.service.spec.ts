import { TestBed, inject } from '@angular/core/testing';

import { WorkQueueResolverService } from './work-queue-resolver.service';

describe('WorkQueueResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkQueueResolverService]
    });
  });

  it('should be created', inject([WorkQueueResolverService], (service: WorkQueueResolverService) => {
    expect(service).toBeTruthy();
  }));
});
