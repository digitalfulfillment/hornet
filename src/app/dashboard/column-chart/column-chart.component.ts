import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-column-chart',
  templateUrl: './column-chart.component.html',
  styleUrls: ['./column-chart.component.scss']
})
export class ColumnChartComponent implements OnInit {
  @Input('categories') categories;
  @Input('title') title;
  @Input('series') series;
  options: any;
  
  constructor() { }

  ngOnInit() {
    this.options = {
      chart: {
          type: 'column'
      },
      
      title: {
        text: this.title,
      },

      plotOptions: {
        column: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: true
        }
      },
      
      series: this.series,

      xAxis: {
        categories: this.categories,
      },

      yAxis: {
        title: {
          text: 'Quality Average'
        }
      },
    }
  }

}
