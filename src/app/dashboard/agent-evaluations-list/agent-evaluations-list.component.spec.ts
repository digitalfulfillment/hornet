import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentEvaluationsListComponent } from './agent-evaluations-list.component';

describe('AgentEvaluationsListComponent', () => {
  let component: AgentEvaluationsListComponent;
  let fixture: ComponentFixture<AgentEvaluationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentEvaluationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentEvaluationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
