import { Component, Inject, OnDestroy } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Agent } from '../../models/agent';
import { Evaluation } from '../../models/evaluation';
import { EvaluationSortingService } from '../../evaluation/evaluation-sorting.service';
import { EvaluationListService } from '../../shared/evaluation-list/evaluation-list.service';

@Component({
  selector: 'app-agent-evaluations-list',
  templateUrl: './agent-evaluations-list.component.html',
  styleUrls: ['./agent-evaluations-list.component.scss'],
  providers: [EvaluationSortingService],
})
export class AgentEvaluationsListComponent implements OnDestroy {
  loading: boolean;
  agent: Agent;
  evaluations: Evaluation[];
  evaluationSubscription: Subscription;

  constructor(
    @Inject(MD_DIALOG_DATA) public data: any,
    private evaluationList: EvaluationListService,
    private sorting: EvaluationSortingService,
    private router: Router,
  ) { 
    this.setData(data);

    this.evaluationSubscription = this.evaluationList.evaluation$.subscribe(evaluation => this.view(evaluation));
  }

  ngOnDestroy() {
    this.evaluationSubscription.unsubscribe();
  }

  setData(data) {
    this.agent = data.agent;
    
    if(data.agent.evaluations.length) this.evaluations = data.agent.evaluations;
    else if(data.agent.evaluations[Object.keys(data.agent.evaluations)[0]]) this.evaluations = [data.agent.evaluations[Object.keys(data.agent.evaluations)[0]]];
    else this.evaluations = [];
  }

  view(evaluation: Evaluation) {
    this.loading = true;
    this.router.navigate([`/evaluation/${evaluation.id}`]);
  }
}
