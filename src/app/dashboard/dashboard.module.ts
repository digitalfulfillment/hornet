import { NgModule } from '@angular/core';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule, MdCardModule, MdDialogModule, MdExpansionModule, MdIconModule, MdInputModule, MdListModule, MdPaginatorModule, MdProgressSpinnerModule, MdSelectModule, MdToolbarModule, MdTooltipModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardDataService } from './dashboard-data.service';
import { WorkQueueDataComponent } from './work-queue-data/work-queue-data.component';
import { AgentEvaluationsListComponent } from './agent-evaluations-list/agent-evaluations-list.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { ChartComponent } from './chart/chart.component';
import { ColumnChartComponent } from './column-chart/column-chart.component';

export declare let require: any;

export function highchartsFactory() {
  const hc = require('highcharts/highstock');
  const dd = require('highcharts/modules/exporting');
  dd(hc);
  return hc;
}

@NgModule({
  imports: [
    ChartModule,
    FormsModule,
    MdButtonModule,
    MdCardModule, 
    MdDialogModule,
    MdExpansionModule,
    MdIconModule, 
    MdInputModule, 
    MdListModule, 
    MdPaginatorModule, 
    MdProgressSpinnerModule,
    MdSelectModule,
    MdToolbarModule, 
    MdTooltipModule,
    ReactiveFormsModule,
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [DashboardComponent, WorkQueueDataComponent, AgentEvaluationsListComponent, LineChartComponent, ChartComponent, ColumnChartComponent],
  entryComponents: [AgentEvaluationsListComponent],
  providers: [
    DashboardDataService, 
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }
  ]
})
export class DashboardModule { }
