import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkQueueDataComponent } from './work-queue-data.component';

describe('WorkQueueDataComponent', () => {
  let component: WorkQueueDataComponent;
  let fixture: ComponentFixture<WorkQueueDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkQueueDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkQueueDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
