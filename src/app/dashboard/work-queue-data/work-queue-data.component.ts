import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MdDialog } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { AgentEvaluationsListComponent } from '../agent-evaluations-list/agent-evaluations-list.component';
import { Agent } from '../../models/agent';
import { Campaign } from '../../models/campaign';
import { DashboardDataService } from '../dashboard-data.service';
import { WorkQueue } from '../../models/work-queue';
import { Team } from '../../models/team';
import { SpinnerService } from '../../core/spinner/spinner.service';

@Component({
  selector: 'app-work-queue-data',
  templateUrl: './work-queue-data.component.html',
  styleUrls: ['./work-queue-data.component.scss']
})
export class WorkQueueDataComponent implements OnInit, OnDestroy {
  campaign: Campaign;
  workQueue: WorkQueue;
  dateRanges: [any];
  teams: Team[];
  routeSubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DashboardDataService,
    private dialog: MdDialog,
    private spinner: SpinnerService,
  ) { }

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.data.subscribe((resp: { data: {
      campaign: Campaign,
      dateRanges: [any],
      teams: Team[],
      work_queue: WorkQueue,
    } }) => this.setData(resp.data));
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  series(team) {
    return team.agents.map(agent => {
      return { name: agent.name, data: agent.scores }
    });
  }

  setData(data) {
    this.campaign = data.campaign;
    this.dateRanges = this.dataService.formatDates(data.dateRanges);
    this.teams = data.teams;
    this.workQueue = data.work_queue;
  }  

  viewEvaluations(agent: Agent) {
    this.dialog.open(AgentEvaluationsListComponent, {
      data: { agent }
    });
  }

  weeks(dateRanges) {
    return dateRanges.map((dateRange, index) => `Week ${index + 1}`);
  }
}
