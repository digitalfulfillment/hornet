import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { WorkQueue } from '../models/work-queue';
import { DashboardDataService } from './dashboard-data.service';
import { SpinnerService } from '../core/spinner/spinner.service';

@Injectable()
export class WorkQueueResolverService {

  constructor(
    private dataService: DashboardDataService,
    private router: Router,
    private spinner: SpinnerService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<any> {
    const work_queue_id = parseInt(route.paramMap.get('workQueueId'));
    const campaign_id = parseInt(route.paramMap.get('campaignId'));
    const month = route.paramMap.get('month');
    const year = route.paramMap.get('year');

    this.spinner.show();
    
    return this.dataService.workQueue({ campaign_id, work_queue_id, month, year })
      .map(resp => {
        this.spinner.hide();
        if(resp) return resp;
        this.router.navigate(['/page-not-found']);
        return null;
      })
      .catch(error => {
        this.spinner.hide();
        this.router.navigate(['/page-not-found']);
        return Observable.throw(error);
      });
  }

}
