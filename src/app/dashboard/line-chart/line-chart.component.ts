import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  @Input('categories') categories;
  @Input('title') title;
  @Input('series') series;
  options: any;

  constructor() { }

  ngOnInit() {
    this.options = {
        title: {
          text: this.title,
      },

      plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: true
        }
    },

      xAxis: {
          categories: this.categories,
      },

      yAxis: {
          title: {
              text: 'Quality Average'
          }
      },

      series: this.series
    }
  }
}
