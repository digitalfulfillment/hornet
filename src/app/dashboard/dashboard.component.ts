import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { DashboardDataService } from './dashboard-data.service';
import { Campaign } from '../models/campaign';
import { ExceptionService } from '../core/exception.service';
import { PusherService } from '../pusher.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  campaigns: Campaign[];
  form: FormGroup;
  dateRanges: string[];
  loading: boolean;
  months: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  years: number[] = [];
  private yearStart: number = 2017;

  formSubscription: Subscription;
  pusherSubscription: Subscription;

  constructor(
    private dataService: DashboardDataService,
    private exception: ExceptionService,
    private pusher: PusherService,
  ) { 
    this.setYears();
  }

  ngOnInit() {
    this.form = new FormGroup({
      month: new FormControl(this.months[new Date().getMonth()]),
      year: new FormControl(this.years[0]),
    })

    this.formSubscription = this.form.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => {
        this.loading = true; 
        return this.fetchData(term).catch(error => {
          this.loading = false;
          this.exception.takeAction(error);
          return Observable.empty();
        })
      })
      .subscribe(
        resp => {
          this.loading = false
          this.setData(resp)
        },
      );
    
    this.pusherSubscription = this.pusher.newNotification$.subscribe(() => this.initData(this.form.value));

    this.initData(this.form.value);
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }
  
  protected fetchData(form: { month: string, year:number }) {
    return this.dataService.index(form);
  }

  protected initData(form: { month: string, year:number }) {
    this.loading = true;
    this.fetchData(form).subscribe(
      resp => this.setData(resp),
      error => this.exception.takeAction(error),
      () => this.loading = false
    );
  }

  series(campaign) {
    return campaign.work_queues.map(workQueue => {
      return { name: workQueue.name, data: workQueue.scores }
    })
  }

  refresh() {
    this.loading = true;
    this.refreshData(this.form.value).subscribe(
      resp => this.setData(resp),
      error => this.exception.takeAction(error),
      () => this.loading = false
    );

  }

  protected refreshData(form: { month: string, year: number }) {
    return this.dataService.refresh(form);
  }

  protected setData(data) {
    this.dateRanges = this.dataService.formatDates(data.dateRanges);
    this.campaigns = data.campaigns;
  }

  protected weeks(dateRanges) {
    return dateRanges.map((dateRange, index) => `Week ${index + 1}`);
  }

  protected setYears() {
    for (let year = new Date().getFullYear(); year >= this.yearStart; year--) {
      this.years.push(year);
    }
  }
}
