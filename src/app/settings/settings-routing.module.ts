import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentsComponent } from './agents/agents.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { SettingsComponent } from './settings.component';
import { TeamsComponent } from './teams/teams.component';
import { WorkQueuesComponent } from './work-queues/work-queues.component';

const routes: Routes = [
  { 
    path: 'agents',
    component: SettingsComponent,
    children: [
      {
        path: '',
        component: AgentsComponent,
      }
    ]
  },
  { 
    path: 'campaigns',
    component: SettingsComponent,
    children: [
      {
        path: '',
        component: CampaignsComponent,
      }
    ]
  },
  { 
    path: 'teams',
    component: SettingsComponent,
    children: [
      {
        path: '',
        component: TeamsComponent,        
      }
    ]
  },
  { 
    path: 'work-queues',
    component: SettingsComponent,
    children: [
      {
        path: '',
        component: WorkQueuesComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
