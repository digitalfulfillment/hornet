import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MdDialog, PageEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Team } from '../../models/team';
import { TeamsDataService } from './teams-data.service';
import { CreateTeamDialogComponent } from './create-team-dialog/create-team-dialog.component';
import { EditTeamDialogComponent } from './edit-team-dialog/edit-team-dialog.component';
import { Settings } from '../settings';
import { SettingsService } from '../settings.service';
import { SpinnerService } from '../../core/spinner/spinner.service';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { Paginated } from '../../interfaces/paginated';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit, OnDestroy, Settings {
  loading: boolean; 
  teams: Team[];
  model: string = 'Team';
  state: string = 'Teams';
  perPage: number;
  total: number;
  createDialogSubscription: Subscription;
  pageEventSubscription: Subscription;
  searchSubscription: Subscription;

  constructor(
    private dataService: TeamsDataService,
    private settings: SettingsService,
    private exception: ExceptionService,
    private pushNotification: PushNotificationService,
    public dialog: MdDialog,
    private spinner: SpinnerService,
  ) {
    this.settings.model = this.model;
    this.settings.state = this.state;

    this.settings.searchField  = new FormControl();
    this.searchSubscription = this.settings.searchField.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => { 
        this.loading = true;
        return this.searchUserInput(term).catch(error => {
          this.loading = false;
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(resp => {
        this.loading = false;
        this.setData(resp);
      });

    this.createDialogSubscription = this.settings.openAddForm().subscribe(() => this.openCreateDialog());
    this.pageEventSubscription = this.settings.getPageEvent().subscribe(pageEvent => this.fetchOnPageEvent(pageEvent));
  }

  ngOnInit() {
    this.loading = true;
    this.initData();
  }

  ngOnDestroy() {
    this.createDialogSubscription.unsubscribe();
    this.pageEventSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
  }

  confirmDelete(team: Team) : void {
    this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: `Delete ${this.model}`,
        message: 'Are you sure you want to delete this team?',
        confirm: 'Delete',
        cancel: 'Cancel',
      }
    }).afterClosed().subscribe(resp => {
      if(resp) {
        this.spinner.show();
        this.deleteData(team)
      };
    });
  }

  deleteData(team: Team) : void {
    this.dataService.delete(team)
      .subscribe(
        () => this.teams.splice(this.teams.indexOf(team), 1),
        err => {
          this.exception.takeAction(err)
          this.spinner.hide();
        },
        () => {
          this.pushNotification.simple(`${this.model} deleted.`);
          this.spinner.hide();
        }
      )
  }

  fetchOnPageEvent(pageEvent: PageEvent) {
    this.loading = true;

    if(this.settings.searchField.value) {
      return this.searchUserInput(this.settings.searchField.value, pageEvent.pageIndex + 1).subscribe(
        resp => this.setData(resp),
        error => {
          this.loading = false;
          this.exception.takeAction(error);
        },
        () => this.loading = false
      )
    }

    this.dataService.paginate(pageEvent.pageIndex + 1).subscribe(
      data => this.setData(data),
      err  => {
        this.loading = false;
        this.exception.takeAction(err);
      },
      () => this.loading = false
    );    
  }

  initData() : void {
    this.dataService.paginate().subscribe(
      data => this.setData(data),
      error => {
        this.loading = false;
        this.exception.takeAction(error);
      },
      () => this.loading = false
    );
  }

  openCreateDialog(): void {
    this.dialog.open(CreateTeamDialogComponent).afterClosed().subscribe(resp => {
      if(resp) {
        if(resp.error) return this.exception.takeAction(resp.error);
        
        this.pushNotification.simple(`${this.model} has been created!`);
        this.initData();
      }
    });
  }

  openEditDialog(team: Team) : void {
    this.dialog.open(EditTeamDialogComponent, {
      data: {
        team: team
      }
    }).afterClosed().subscribe(resp => {
      if(resp) {
        if(resp.error) return this.exception.takeAction(resp.error);
        
        this.pushNotification.simple('Changes saved!');
        this.initData();
      }
    })
  }

  searchUserInput(term: string, page?: number): Observable<Paginated> {
    let query = { 
      employee_number: term,
      name: term,
    }
    
    return this.dataService.search(query, page);
  }

  setData(data) {
    this.teams = data.data;
    this.settings.perPage = data.per_page;
    this.settings.total = data.total;
  }
}
