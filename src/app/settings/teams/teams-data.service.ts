import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '../../core/api.service';
import { Team } from '../../models/team';
import { Paginated } from '../../interfaces/paginated';

const url = 'team';

@Injectable()
export class TeamsDataService {

  constructor(private api: ApiService) { }
  
  checkDuplicate(team: Team) : Observable<any> {
    return this.api.post(`${url}/check-duplicate`, team);
  }

  delete(team: Team): Observable<void> {
    return this.api.delete(`${url}/${team.id}`);
  }

  paginate(page?: number): Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`);
  }

  search(query: object, page?: number): Observable<Paginated> {
    return this.api.post(`${url}/search?page=${page}`, query);
  }

  store(team: Team) {
    return this.api.post(`${url}`, team);
  }
  
  update(team: Team) {
    return this.api.put(`${url}/${team.id}`, team);
  }
}
