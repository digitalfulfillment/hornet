import { TestBed, inject } from '@angular/core/testing';

import { TeamsDialogService } from './teams-dialog.service';

describe('TeamsDialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeamsDialogService]
    });
  });

  it('should be created', inject([TeamsDialogService], (service: TeamsDialogService) => {
    expect(service).toBeTruthy();
  }));
});
