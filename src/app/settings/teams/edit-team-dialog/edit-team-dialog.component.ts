import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Team } from '../../../models/team';
import { TeamsDataService } from '../teams-data.service';
import { TeamsDialogService } from '../teams-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-edit-team-dialog',
  templateUrl: './edit-team-dialog.component.html',
  styleUrls: ['./edit-team-dialog.component.scss'],
  providers: [TeamsDialogService]
})
export class EditTeamDialogComponent implements OnInit, OnDestroy {
  busy: boolean;
  hasDuplicate: boolean;
  team: Team;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<EditTeamDialogComponent>,
    public editTeamForm: TeamsDialogService,
    private teamsData: TeamsDataService,
    @Inject(MD_DIALOG_DATA) public data:any
  ) { 
    this.team = this.data.team;  
    this.editTeamForm.formGroup.setValue({
      name: this.team.name,
    });
  }

  ngOnInit() {
    this.searchSubscription = this.editTeamForm.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(name : string) : Observable<Paginated> {
    let team = new Team({
      id: this.team.id,
      name: name
    })

    return this.teamsData.checkDuplicate(team);
  }

  submit() {
    if(this.editTeamForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let team = new Team({
        id: this.team.id,          
        name: this.editTeamForm.formGroup.get('name').value, 
      });
        
      this.teamsData.update(team).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }
}
