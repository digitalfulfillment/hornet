import { Component, OnInit, OnDestroy } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Team } from '../../../models/team';
import { TeamsDataService } from '../teams-data.service';
import { TeamsDialogService } from '../teams-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-create-team-dialog',
  templateUrl: './create-team-dialog.component.html',
  styleUrls: ['./create-team-dialog.component.scss'],
  providers: [TeamsDialogService]
})
export class CreateTeamDialogComponent implements OnInit, OnDestroy {
  busy: boolean;
  hasDuplicate: boolean;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<CreateTeamDialogComponent>,
    public createTeamForm: TeamsDialogService,
    private teamsData: TeamsDataService,
  ) { }

  ngOnInit() {
    this.searchSubscription = this.createTeamForm.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(name : string) : Observable<Paginated> {
    let team = new Team({
      name: name
    });

    return this.teamsData.checkDuplicate(team);
  }

  submit() {
    if(this.createTeamForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let team = new Team(
        { name: this.createTeamForm.formGroup.get('name').value }
      );

      this.teamsData.store(team).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }

}
