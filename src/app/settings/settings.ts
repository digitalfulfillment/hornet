import { Subscription } from 'rxjs/Subscription';
import { PageEvent } from '@angular/material';
import { Observable } from 'rxjs';

import { Paginated } from '../interfaces/paginated';

export interface Settings {
    model: string;
    perPage : number;
    state: string;
    total: number;
    pageEventSubscription: Subscription;
    createDialogSubscription: Subscription;
    searchSubscription: Subscription;

    confirmDelete(any: any) : void;
    deleteData(any: any): void;
    fetchOnPageEvent(pageEvent: PageEvent): void;
    initData() : void;
    openCreateDialog(): void;
    openEditDialog(any: any): void;
    searchUserInput(term: string) : Observable<Paginated>;
}
