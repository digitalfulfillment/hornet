import { Component, OnInit, OnDestroy } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Campaign } from '../../../models/campaign';
import { CampaignsDataService } from '../campaigns-data.service';
import { CampaignsDialogService } from '../campaigns-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-create-campaign-dialog',
  templateUrl: './create-campaign-dialog.component.html',
  styleUrls: ['./create-campaign-dialog.component.scss'],
  providers: [CampaignsDialogService]
})
export class CreateCampaignDialogComponent implements OnInit {
  busy: boolean;
  hasDuplicate: boolean;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<CreateCampaignDialogComponent>,
    public createCampaignForm: CampaignsDialogService,
    private campaignsData: CampaignsDataService,
  ) { }

  ngOnInit() {
    this.searchSubscription = this.createCampaignForm.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(name : string) : Observable<Paginated> {
    let campaign = new Campaign({
      name: name
    });

    return this.campaignsData.checkDuplicate(campaign);
  }

  submit() {
    if(this.createCampaignForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let campaign = new Campaign(
        { name: this.createCampaignForm.formGroup.get('name').value }
      );

      this.campaignsData.store(campaign).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }

}
