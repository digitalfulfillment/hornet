import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCampaignDialogComponent } from './create-campaign-dialog.component';

describe('CreateCampaignDialogComponent', () => {
  let component: CreateCampaignDialogComponent;
  let fixture: ComponentFixture<CreateCampaignDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCampaignDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCampaignDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
