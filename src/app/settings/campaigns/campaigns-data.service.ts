import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '../../core/api.service';
import { Campaign } from '../../models/campaign';
import { Paginated } from '../../interfaces/paginated';

const url = 'campaign';

@Injectable()
export class CampaignsDataService {

  constructor(private api: ApiService) { }

  checkDuplicate(campaign: Campaign) : Observable<any> {
    return this.api.post(`${url}/check-duplicate`, campaign);
  }

  delete(campaign: Campaign): Observable<void> {
    return this.api.delete(`${url}/${campaign.id}`);
  }

  paginate(page?: number): Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`);
  }

  search(query: object, page?: number): Observable<Paginated> {
    return this.api.post(`${url}/search?page=${page}`, query);
  }

  show(id: number): Observable<Campaign> {
    return this.api.get(`${url}/${id}`);
  }

  store(campaign: Campaign) {
    return this.api.post(`${url}`, campaign);
  }
  
  update(campaign: Campaign) {
    return this.api.put(`${url}/${campaign.id}`, campaign);
  }
}
