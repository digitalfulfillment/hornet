import { TestBed, inject } from '@angular/core/testing';

import { CampaignsDialogService } from './campaigns-dialog.service';

describe('CampaignsDialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CampaignsDialogService]
    });
  });

  it('should be created', inject([CampaignsDialogService], (service: CampaignsDialogService) => {
    expect(service).toBeTruthy();
  }));
});
