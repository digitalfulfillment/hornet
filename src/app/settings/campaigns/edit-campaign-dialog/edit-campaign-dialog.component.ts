import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Campaign } from '../../../models/campaign';
import { CampaignsDataService } from '../campaigns-data.service';
import { CampaignsDialogService } from '../campaigns-dialog.service';
import { ExceptionService } from '../../../core/exception.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-edit-campaign-dialog',
  templateUrl: './edit-campaign-dialog.component.html',
  styleUrls: ['./edit-campaign-dialog.component.scss'],
  providers: [CampaignsDialogService],
})
export class EditCampaignDialogComponent implements OnInit {
  busy: boolean;
  hasDuplicate: boolean;
  campaign: Campaign;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<EditCampaignDialogComponent>,
    public editCampaignForm: CampaignsDialogService,
    private campaignsData: CampaignsDataService,
    private exception: ExceptionService,
    @Inject(MD_DIALOG_DATA) public data:any 
  ) { 
    this.campaign = this.data.campaign;  
    this.editCampaignForm.formGroup.setValue({
      name: this.campaign.name,
    });
  }

  ngOnInit() {
    this.searchSubscription = this.editCampaignForm.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(name : string) : Observable<Paginated> {
    let campaign = new Campaign({
      id: this.campaign.id,
      name: name
    })

    return this.campaignsData.checkDuplicate(campaign);
  }

  submit() {
    if(this.editCampaignForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let campaign = new Campaign({
        id: this.campaign.id,          
        name: this.editCampaignForm.formGroup.get('name').value, 
      });
        
      this.campaignsData.update(campaign).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }

}
