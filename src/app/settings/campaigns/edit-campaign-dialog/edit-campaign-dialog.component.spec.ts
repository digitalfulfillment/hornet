import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCampaignDialogComponent } from './edit-campaign-dialog.component';

describe('EditCampaignDialogComponent', () => {
  let component: EditCampaignDialogComponent;
  let fixture: ComponentFixture<EditCampaignDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCampaignDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCampaignDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
