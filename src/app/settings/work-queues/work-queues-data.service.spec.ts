import { TestBed, inject } from '@angular/core/testing';

import { WorkQueuesDataService } from './work-queues-data.service';

describe('WorkQueuesDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkQueuesDataService]
    });
  });

  it('should be created', inject([WorkQueuesDataService], (service: WorkQueuesDataService) => {
    expect(service).toBeTruthy();
  }));
});
