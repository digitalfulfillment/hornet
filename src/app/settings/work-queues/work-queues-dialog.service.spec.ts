import { TestBed, inject } from '@angular/core/testing';

import { WorkQueuesDialogService } from './work-queues-dialog.service';

describe('WorkQueuesDialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkQueuesDialogService]
    });
  });

  it('should be created', inject([WorkQueuesDialogService], (service: WorkQueuesDialogService) => {
    expect(service).toBeTruthy();
  }));
});
