import { Component, OnInit, OnDestroy } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { WorkQueue } from '../../../models/work-queue';
import { WorkQueuesDataService } from '../work-queues-data.service';
import { WorkQueuesDialogService } from '../work-queues-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-create-work-queue-dialog',
  templateUrl: './create-work-queue-dialog.component.html',
  styleUrls: ['./create-work-queue-dialog.component.scss'],
  providers:[WorkQueuesDialogService]
})
export class CreateWorkQueueDialogComponent implements OnInit {
  busy: boolean;
  hasDuplicate: boolean;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<CreateWorkQueueDialogComponent>,
    public createWorkQueueForm: WorkQueuesDialogService,
    private workQueuesData: WorkQueuesDataService,
  ) { }

  ngOnInit() {
    this.searchSubscription = this.createWorkQueueForm.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(name : string) : Observable<Paginated> {
    let workQueue = new WorkQueue({
      name: name
    });

    return this.workQueuesData.checkDuplicate(workQueue);
  }

  submit() {
    if(this.createWorkQueueForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let workQueue = new WorkQueue(
        { name: this.createWorkQueueForm.formGroup.get('name').value }
      );

      this.workQueuesData.store(workQueue).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }
}
