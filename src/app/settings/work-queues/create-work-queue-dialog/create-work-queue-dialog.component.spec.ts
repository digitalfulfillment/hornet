import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWorkQueueDialogComponent } from './create-work-queue-dialog.component';

describe('CreateWorkQueueDialogComponent', () => {
  let component: CreateWorkQueueDialogComponent;
  let fixture: ComponentFixture<CreateWorkQueueDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWorkQueueDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWorkQueueDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
