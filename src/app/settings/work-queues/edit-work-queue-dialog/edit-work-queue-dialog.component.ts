import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { WorkQueue } from '../../../models/work-queue';
import { WorkQueuesDataService } from '../work-queues-data.service';
import { WorkQueuesDialogService } from '../work-queues-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-edit-work-queue-dialog',
  templateUrl: './edit-work-queue-dialog.component.html',
  styleUrls: ['./edit-work-queue-dialog.component.scss'],
  providers:[WorkQueuesDialogService]
})
export class EditWorkQueueDialogComponent implements OnInit {
  busy: boolean;
  hasDuplicate: boolean;
  workQueue: WorkQueue;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<EditWorkQueueDialogComponent>,
    public editWorkQueueForm: WorkQueuesDialogService,
    private workQueuesData: WorkQueuesDataService,
    @Inject(MD_DIALOG_DATA) public data:any
  ) { 
    this.workQueue = this.data.workQueue;  
    this.editWorkQueueForm.formGroup.setValue({
      name: this.workQueue.name,
    });
  }

  ngOnInit() {
    this.searchSubscription = this.editWorkQueueForm.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(name : string) : Observable<Paginated> {
    let workQueue = new WorkQueue({
      id: this.workQueue.id,
      name: name
    })

    return this.workQueuesData.checkDuplicate(workQueue);
  }

  submit() {
    if(this.editWorkQueueForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let workQueue = new WorkQueue({
        id: this.workQueue.id,          
        name: this.editWorkQueueForm.formGroup.get('name').value, 
      });
        
      this.workQueuesData.update(workQueue).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }

}
