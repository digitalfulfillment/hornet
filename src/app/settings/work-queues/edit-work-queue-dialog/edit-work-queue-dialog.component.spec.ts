import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWorkQueueDialogComponent } from './edit-work-queue-dialog.component';

describe('EditWorkQueueDialogComponent', () => {
  let component: EditWorkQueueDialogComponent;
  let fixture: ComponentFixture<EditWorkQueueDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditWorkQueueDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditWorkQueueDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
