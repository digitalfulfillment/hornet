import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '../../core/api.service';
import { WorkQueue } from '../../models/work-queue';
import { Paginated } from '../../interfaces/paginated';

const url = 'work-queue';

@Injectable()
export class WorkQueuesDataService {

  constructor(private api: ApiService) { }
  
  checkDuplicate(workQueue: WorkQueue) : Observable<any> {
    return this.api.post(`${url}/check-duplicate`, workQueue);
  }

  delete(workQueue: WorkQueue): Observable<void> {
    return this.api.delete(`${url}/${workQueue.id}`);
  }

  paginate(page?: number): Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`);
  }

  search(query: object, page?: number): Observable<Paginated> {
    return this.api.post(`${url}/search?page=${page}`, query);
  }

  show(id: number): Observable<WorkQueue> {
    return this.api.get(`${url}/${id}`);
  }

  store(workQueue: WorkQueue) {
    return this.api.post(`${url}`, workQueue);
  }
  
  update(workQueue: WorkQueue) {
    return this.api.put(`${url}/${workQueue.id}`, workQueue);
  }

}
