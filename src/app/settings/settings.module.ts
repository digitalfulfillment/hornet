import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule, MdCardModule, MdDialogModule, MdIconModule, MdInputModule, MdListModule, MdMenuModule, MdPaginatorModule, MdProgressSpinnerModule, MdToolbarModule, MdTooltipModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';
import { SettingsRoutingModule } from './settings-routing.module';
import { AgentsComponent } from './agents/agents.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { TeamsComponent } from './teams/teams.component';
import { WorkQueuesComponent } from './work-queues/work-queues.component';
import { SettingsComponent } from './settings.component';
import { SettingsService } from './settings.service';
import { CreateAgentDialogComponent } from './agents/create-agent-dialog/create-agent-dialog.component';
import { EditAgentDialogComponent } from './agents/edit-agent-dialog/edit-agent-dialog.component';
import { CreateCampaignDialogComponent } from './campaigns/create-campaign-dialog/create-campaign-dialog.component';
import { EditCampaignDialogComponent } from './campaigns/edit-campaign-dialog/edit-campaign-dialog.component';
import { CreateTeamDialogComponent } from './teams/create-team-dialog/create-team-dialog.component';
import { EditTeamDialogComponent } from './teams/edit-team-dialog/edit-team-dialog.component';
import { CreateWorkQueueDialogComponent } from './work-queues/create-work-queue-dialog/create-work-queue-dialog.component';
import { EditWorkQueueDialogComponent } from './work-queues/edit-work-queue-dialog/edit-work-queue-dialog.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MdButtonModule,
    MdCardModule,
    MdDialogModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdPaginatorModule,
    MdProgressSpinnerModule,
    MdToolbarModule,
    MdTooltipModule,
    SharedModule,
    SettingsRoutingModule
  ],
  declarations: [
    AgentsComponent,
    CampaignsComponent, 
    TeamsComponent, 
    WorkQueuesComponent, 
    SettingsComponent, 
    CreateAgentDialogComponent, 
    EditAgentDialogComponent, 
    CreateCampaignDialogComponent, 
    EditCampaignDialogComponent, 
    CreateTeamDialogComponent, 
    EditTeamDialogComponent,
    CreateWorkQueueDialogComponent,
    EditWorkQueueDialogComponent
  ],
  entryComponents: [
    CreateAgentDialogComponent,
    EditAgentDialogComponent, 
    CreateCampaignDialogComponent, 
    EditCampaignDialogComponent, 
    CreateTeamDialogComponent,
    EditTeamDialogComponent,
    CreateWorkQueueDialogComponent,
    EditWorkQueueDialogComponent
  ],
  providers: [SettingsService]
})
export class SettingsModule { }
