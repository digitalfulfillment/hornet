import { Injectable } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { FormValidationMessage } from '../../core/form-validation-message';
import { FormValidationMessageService } from '../../core/form-validation-message.service';

@Injectable()
export class AgentsDialogService implements FormValidationMessage {
  formGroup: FormGroup;

  errors: { employeeNumber, firstName, lastName } = {
    'employeeNumber': '',
    'firstName': '',
    'lastName': '',
  };

  rules: { employeeNumber, firstName, lastName } = {
    'employeeNumber' : ['', [
        Validators.required,
      ]
    ],
    'firstName': ['', [
        Validators.required,
        Validators.pattern('^[^0-9]+$')
      ]
    ],
    'lastName': ['', [
        Validators.required,
        Validators.pattern('^[^0-9]+$')
      ]
    ],
  }

  validationMessages: { employeeNumber, firstName, lastName } = {
    'employeeNumber': {
      'required': 'Employee number is required.',
    },
    'firstName': {
      'required': 'First name is required.',
      'pattern': 'First name cannot contain numbers'
    },
    'lastName': {
      'required': 'First name is required.',
      'pattern': 'Last name cannot contain numbers'
    },
  }

  constructor(private form: FormValidationMessageService) { 
    form.rules = this.rules;
    form.errors = this.errors;
    form.validationMessages = this.validationMessages;

    this.form.buildForm();
    this.formGroup = this.form.formGroup;
  }
}
