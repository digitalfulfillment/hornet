import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MdDialog, PageEvent } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Agent } from '../../models/agent';
import { AgentsDataService } from './agents-data.service';
import { CreateAgentDialogComponent } from './create-agent-dialog/create-agent-dialog.component';
import { Settings } from '../settings';
import { SettingsService } from '../settings.service';
import { SpinnerService } from '../../core/spinner/spinner.service';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { Paginated } from '../../interfaces/paginated';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { EditAgentDialogComponent } from './edit-agent-dialog/edit-agent-dialog.component'; 

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.scss']
})
export class AgentsComponent implements OnInit, OnDestroy, Settings {
  loading: boolean;
  agents : Agent[] = [];
  model: string = 'Agent';
  state: string = 'Agents';
  perPage: number;
  total: number;
  createDialogSubscription: Subscription;
  pageEventSubscription: Subscription;
  searchSubscription: Subscription;

  constructor(
    private dataService: AgentsDataService,
    private settings: SettingsService,
    private exception: ExceptionService,
    private pushNotification: PushNotificationService,
    public dialog: MdDialog,
    private spinner: SpinnerService,
  ) { 
      this.settings.model = this.model;
      this.settings.state = this.state;

      this.settings.searchField  = new FormControl();
      this.searchSubscription = this.settings.searchField.valueChanges
        .debounceTime(400)
        .distinctUntilChanged()
        .switchMap(term => {
          this.loading = true;

          return this.searchUserInput(term).catch(error => {
            this.loading = false;
            this.exception.takeAction(error);
            return Observable.empty();
          })
        })
        .subscribe(resp => {
          this.loading = false;
          this.setData(resp);
        });

      this.createDialogSubscription = this.settings.openAddForm().subscribe(() => this.openCreateDialog());
      this.pageEventSubscription = this.settings.getPageEvent().subscribe(pageEvent => this.fetchOnPageEvent(pageEvent));
  }

  ngOnInit() {
    this.loading = true;
    this.initData();
  }

  ngOnDestroy() {
    this.createDialogSubscription.unsubscribe();
    this.pageEventSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
  }

  confirmDelete(agent: Agent) : void {
    this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: `Delete ${this.model}`,
        message: 'Are you sure you want to delete this agent?',
        confirm: 'Delete',
        cancel: 'Cancel',
      }
    }).afterClosed().subscribe(resp => {
      if(resp) {
        this.spinner.show();
        this.deleteData(agent);
      }
    });
  }

  deleteData(agent: Agent) : void {
    this.dataService.delete(agent)
      .subscribe(
        () => this.agents.splice(this.agents.indexOf(agent), 1),
        err => {
          this.spinner.hide();
          this.exception.takeAction(err);
        },
        () => {
          this.spinner.hide();
          this.pushNotification.simple(`${this.model} deleted.`);
        }
      );
  }

  fetchOnPageEvent(pageEvent : PageEvent) {
    this.loading = true;

    if(this.settings.searchField.value) {
      return this.searchUserInput(this.settings.searchField.value, pageEvent.pageIndex + 1).subscribe(
        data => this.setData(data),
        error => {
          this.loading = false;
          this.exception.takeAction(error)
        },
        () => this.loading = false
      );
    }
    
    this.dataService.paginate(pageEvent.pageIndex + 1).subscribe(
      data => this.setData(data),
      error => {
        this.loading = false;
        this.exception.takeAction(error)
      },
      () => this.loading = false
    );
  }

  initData() : void {
    this.dataService.paginate().subscribe(
      data => this.setData(data),
      error => {
        this.loading = false;
        this.exception.takeAction(error)
      },
      () => this.loading = false
    );
  }

  setData(data) {
    this.agents = data.data;
    this.settings.perPage = data.per_page;
    this.settings.total = data.total;
  }

  openCreateDialog() : void {
    this.dialog.open(CreateAgentDialogComponent).afterClosed().subscribe(resp => {
      if(resp) {
        if(resp.error) return this.exception.takeAction(resp.error);
        
        this.pushNotification.simple(`${this.model} has been created!`);
        this.initData();
      }
    });
  }

  openEditDialog(agent: Agent) : void {
    this.dialog.open(EditAgentDialogComponent, {
      data: {
        agent: agent
      }
    }).afterClosed().subscribe(resp => {
      if(resp) {
        if(resp.error) return this.exception.takeAction(resp.error);

        this.pushNotification.simple('Changes saved!');
        this.initData();
      }
    })
  }

  searchUserInput(term : string, page?: number): Observable<Paginated> {
    let query = { 
      employee_number: term,
      name: term,
    }
    
    return this.dataService.search(query, page)
  }
}
