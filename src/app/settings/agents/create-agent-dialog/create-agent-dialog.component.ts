import { Component, OnInit, OnDestroy } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Agent } from '../../../models/Agent';
import { AgentsDataService } from '../agents-data.service';
import { AgentsDialogService } from '../agents-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-create-agent-dialog',
  templateUrl: './create-agent-dialog.component.html',
  styleUrls: ['./create-agent-dialog.component.scss'],
  providers:[AgentsDialogService]
})
export class CreateAgentDialogComponent implements OnInit, OnDestroy {
  busy: boolean;
  hasDuplicate: boolean;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<CreateAgentDialogComponent>,
    public createAgentForm: AgentsDialogService,
    private agentsData: AgentsDataService,
  ) { }

  ngOnInit() {
    this.searchSubscription = this.createAgentForm.formGroup.get('employeeNumber').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(employeeNumber => this.checkDuplicate(employeeNumber).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty()
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  private checkDuplicate(employeeNumber : string) : Observable<Paginated> {
    let agent = new Agent({
      employee_number: employeeNumber
    })

    return this.agentsData.checkDuplicate(agent);
  }

  submit() {
    if(this.createAgentForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let agent = new Agent({
        employee_number: this.createAgentForm.formGroup.get('employeeNumber').value,
        name: `${this.createAgentForm.formGroup.get('lastName').value}, ${this.createAgentForm.formGroup.get('firstName').value}`        
      });

      this.agentsData.store(agent).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true)
      );
    }
  }
}
