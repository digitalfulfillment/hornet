import { TestBed, inject } from '@angular/core/testing';

import { AgentsDialogService } from './agents-dialog.service';

describe('AgentsDialogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgentsDialogService]
    });
  });

  it('should be created', inject([AgentsDialogService], (service: AgentsDialogService) => {
    expect(service).toBeTruthy();
  }));
});
