import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { Agent } from '../../models/agent';
import { ApiService } from '../../core/api.service';
import { Paginated } from '../../interfaces/paginated';

const url = 'agent';

@Injectable()
export class AgentsDataService {

  constructor(private api: ApiService) { }

  checkDuplicate(agent: Agent) : Observable<any> {
    return this.api.post(`${url}/check-duplicate`, agent);
  }

  delete(agent: Agent) : Observable<void> {
    return this.api.delete(`${url}/${agent.id}`);
  }

  paginate(page?: number) : Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`);
  }

  search(query: { employee_number?: string, name?: string }, page?: number) : Observable<Paginated> {
    return this.api.post(`${url}/search?page=${page}`, query);
  }

  store(agent: Agent) : Observable<void> {
    return this.api.post(`${url}`, agent);
  }

  update(agent: Agent) : Observable<void> {
    return this.api.put(`${url}/${agent.id}`, agent);
  }
}
