import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Agent } from '../../../models/Agent';
import { AgentsDataService } from '../agents-data.service';
import { AgentsDialogService } from '../agents-dialog.service';
import { Paginated } from '../../../interfaces/paginated';

@Component({
  selector: 'app-edit-agent-dialog',
  templateUrl: './edit-agent-dialog.component.html',
  styleUrls: ['./edit-agent-dialog.component.scss'],
  providers:[AgentsDialogService]
})
export class EditAgentDialogComponent implements OnInit, OnDestroy {
  busy: boolean;
  hasDuplicate: boolean;
  agent: Agent;
  searchSubscription: Subscription;

  constructor(
    public dialogRef: MdDialogRef<EditAgentDialogComponent>,
    public editAgentForm: AgentsDialogService,
    private agentsData: AgentsDataService,
    @Inject(MD_DIALOG_DATA) public data:any 
  ) {
    this.agent = this.data.agent;  
    let name = this.agent.name.split(', ');
    this.editAgentForm.formGroup.setValue({
      employeeNumber: this.agent.employee_number,
      firstName: name[1],
      lastName: name[0],
    });
  }

  ngOnInit() {
    this.searchSubscription = this.editAgentForm.formGroup.get('employeeNumber').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(employeeNumber =>  this.checkDuplicate(employeeNumber).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
  }

  checkDuplicate(employeeNumber : string) : Observable<Paginated> {
    let agent = new Agent({
      id: this.agent.id,
      employee_number: employeeNumber
    });

    return this.agentsData.checkDuplicate(agent);
  }

  submit() {
    if(this.editAgentForm.formGroup.valid && !this.busy && !this.hasDuplicate) {
      this.busy = true;

      let agent = new Agent({
        id: this.agent.id,
        employee_number: this.editAgentForm.formGroup.get('employeeNumber').value,
        name: `${this.editAgentForm.formGroup.get('lastName').value}, ${this.editAgentForm.formGroup.get('firstName').value}`       
      });

      this.agentsData.update(agent).subscribe(
        () => this.busy = false,
        error => { 
          this.dialogRef.close({ error });
          this.busy = false; 
        },
        () => this.dialogRef.close(true),
      );
    }
  }
}
