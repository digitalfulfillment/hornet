import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';

import { ApiService } from '../../core/api.service';
import { AuthService } from '../../auth/auth.service';
import { ExceptionService } from '../../core/exception.service';
import { SidenavService } from '../sidenav.service';
import { ChangePasswordComponent } from '../../auth/change-password/change-password.component';
import { PushNotificationService } from '../../core/push-notification.service';
import { PusherService } from '../../pusher.service';
import { environment } from '../../../environments/environment'; 

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  user : string;
  name: string;

  constructor(
    private api: ApiService,
    private authService: AuthService,
    private dialog: MdDialog,
    private exception: ExceptionService,
    private sidenavService:SidenavService,
    private pusher: PusherService,
    private pushNotification: PushNotificationService,
  ) { }

  ngOnInit() {
    this.name = environment.name;
    this.user = this.authService.user.name;
  }

  toggleSidenav() {
    this.sidenavService.toggle();
  }

  changePassword() {
    this.dialog.open(ChangePasswordComponent).afterClosed().subscribe(resp => {
      if(resp) {
        if(resp.error) return this.exception.takeAction(resp.error);
        this.pushNotification.simple('Password changed!');
      }
    })
  }

  logout() {
    this.pusher.disconnect();
    this.authService.logout();
  }

  testError() {
    this.api.post('exception-report/test').subscribe(
      () => console.log('No error.'),
      error => this.exception.takeAction(error),
    );
  }
}
