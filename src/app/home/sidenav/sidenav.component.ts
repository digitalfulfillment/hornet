import { Component, ViewChild, HostListener, OnInit } from '@angular/core';
import { MdSidenav } from '@angular/material';

import { AuthService } from '../../auth/auth.service';
import { SidenavService } from '../sidenav.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit{
  canComposeQuestionnaires: boolean;
  canEvaluate: boolean;
  canManageSettings: boolean;
  canManageAccounts: boolean;

  @ViewChild('sidenav') sidenav: MdSidenav
  @HostListener('window:resize', ['$event'])
  onResize(event) {
      if (event.target.innerWidth < 500) {
          this.sidenav.close();
      }
      else{
        this.sidenav.open();
      }
  }

  constructor(private sidenavService: SidenavService, private authService: AuthService) { }

  ngOnInit(){
    this.sidenavService.sidenav = this.sidenav;
    // Todo: check user roles and display allowed modules
    if(this.authService.user.is_admin) 
    {
      this.canEvaluate = true;
      this.canComposeQuestionnaires = true;
      this.canManageSettings = true;
      this.canManageAccounts = true;
    }
    else {
      this.canComposeQuestionnaires = this.authService.user.roles.filter(role => role.name == 'compose-questionnaires').length ? true: false;
      this.canManageSettings = this.authService.user.roles.filter(role => role.name == 'manage-settings').length ? true: false;
      this.canEvaluate = this.authService.user.roles.filter(role => role.name == 'manage-reports' || role.name == 'delete-reports' || role.name == 'view-all-reports').length ? true: false;
      this.canManageAccounts = this.authService.user.roles.filter(role => role.name == 'manage-users').length ? true: false;
    }
  }
}
