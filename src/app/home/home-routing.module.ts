import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { AuthGuard } from '../auth/auth.guard';
import { EvaluationGuard } from '../auth/roles/evaluation.guard';
import { QuestionnaireGuard } from '../auth/roles/questionnaire.guard';
import { SettingsGuard } from '../auth/roles/settings.guard';
import { UserGuard } from '../auth/roles/user.guard';

const routes: Routes = [
  { 
    path: '', 
    component: HomeComponent,
    canActivate:[AuthGuard],
    children: [
      {
        path:'',
        loadChildren: 'app/dashboard/dashboard.module#DashboardModule',
        canLoad: [AuthGuard],
        canActivateChild: [AuthGuard],
      },
      {
        path:'evaluation',
        loadChildren: 'app/evaluation/evaluation.module#EvaluationModule',
        canLoad: [AuthGuard, EvaluationGuard],
        canActivateChild: [AuthGuard, EvaluationGuard],
      },
      {
        path:'questionnaires',
        loadChildren: 'app/questionnaire/questionnaire.module#QuestionnaireModule',
        canLoad: [AuthGuard, QuestionnaireGuard],
        canActivateChild: [AuthGuard, QuestionnaireGuard],
      },
      {
        path:'settings',
        loadChildren: 'app/settings/settings.module#SettingsModule',
        canLoad: [AuthGuard, SettingsGuard],
        canActivateChild: [AuthGuard, SettingsGuard],
      },
      {
        path:'users',
        loadChildren: 'app/user/user.module#UserModule',
        canLoad: [AuthGuard, UserGuard],
        canActivateChild: [AuthGuard, UserGuard],
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
