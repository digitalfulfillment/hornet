import { Component, OnInit } from '@angular/core';

import { PusherService } from '../pusher.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private pusher: PusherService) { }

  ngOnInit() {
    this.pusher.init();
  }
}
