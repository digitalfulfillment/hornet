import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule, MdListModule, MdMenuModule, MdSidenavModule, MdToolbarModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { SidenavService } from './sidenav.service';
import { PusherService } from '../pusher.service';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MdButtonModule,
    MdListModule,
    MdMenuModule,
    MdSidenavModule,
    MdToolbarModule,
    SharedModule,
    HomeRoutingModule,
  ],
  declarations: [HomeComponent, ToolbarComponent, SidenavComponent],
  providers: [PusherService, SidenavService]
})
export class HomeModule { }
