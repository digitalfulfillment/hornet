import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '../core/api.service';
import { Paginated } from '../interfaces/paginated';
import { User } from '../models/user';

const url = 'user';

@Injectable()
export class UserDataService {

  constructor(private api: ApiService) { }

  checkDuplicate(query): Observable<boolean> {
    return this.api.post(`${url}/check-duplicate`, query);
  }

  paginate(page?: number): Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`);
  }

  search(query): Observable<Paginated> {
    return this.api.post(`${url}/search`, query);
  }

  store(data): Observable<User> {
    return this.api.post(`${url}`, data);
  }

  show(id: number): Observable<User> {
    return this.api.get(`${url}/${id}`);
  }

  delete(id: number) {
    return this.api.delete(`${url}/${id}`);
  }

  update(id: number, user) {
    return this.api.put(`${url}/${id}`, user);
  }
}
