import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ExceptionService } from '../../../core/exception.service';
import { Role } from '../../../models/role';
import { SpinnerService } from '../../../core/spinner/spinner.service';
import { User } from '../../../models/user';
import { UserFormService } from '../user-form.service';
import { UserDataService } from '../../user-data.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
  providers: [UserFormService],
})
export class CreateUserComponent implements OnInit {
  activatedRouteSubscription: Subscription;
  user: User;
  userSubscription: Subscription;
  roles: Role[];
  label: string = 'New User';

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: UserDataService,
    private exception: ExceptionService,
    private router: Router,
    private spinner: SpinnerService,
    private userForm: UserFormService,
  ) { }

  ngOnInit() {
    this.activatedRouteSubscription = this.activatedRoute.data.subscribe((data: { roles:Role[] }) => this.setRoles(data.roles));

    this.userSubscription = this.userForm.form$.subscribe(() => this.save(this.payload()));
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.activatedRouteSubscription.unsubscribe();
  }

  canDeactivate() : Observable<boolean> | boolean {
    return this.userForm.confirmDiscardChanges();
  }

  payload() {
    let user = this.userForm.formGroup.value;
    user.roles = this.rolesPayload();

    return user;
  }

  rolesPayload() {
    let roles = [];
    
    this.roles.forEach((role, index) => {
      if(this.userForm.formGroup.get('roles').value[index].role) roles.push(role.id);
    });

    return roles;
  }

  setRoles(roles) {
    this.roles = roles;
    this.roles.forEach(role => this.userForm.addRole());
  }

  save(data) {
    this.userForm.busy = true;
    this.dataService.store(data).subscribe(
      resp => this.userForm.submitted = true,
      error => {
        this.userForm.busy = false;
        this.exception.takeAction(error)
      },
      () => {
        this.userForm.busy = false;
        this.router.navigate(['/users']);
      }
    );
  }
}
