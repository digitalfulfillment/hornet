import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Role } from '../../models/role';
import { User } from '../../models/user';
import { UserFormService } from './user-form.service';
import { UserDataService } from '../user-data.service';
import { AlertDialogService } from '../../core/alert-dialog/alert-dialog.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit, OnDestroy {
  @Input('label') label: string;
  @Input('roles') roles: Role[];
  @Input('user') user: User;
  emailSubscription: Subscription;
  passwordSubscription: Subscription;
  passwordConfirmationSubscription: Subscription;

  constructor(
    private alert: AlertDialogService,
    private dataService: UserDataService,
    public userForm: UserFormService) { }

  ngOnInit() {
    this.emailSubscription = this.userForm.formGroup.controls['email'].valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => this.checkDuplicate(term).catch(error => Observable.empty()))
      .subscribe((resp: any) => this.userForm.hasDuplicate = resp);

    this.passwordSubscription = this.userForm.formGroup.controls['password'].valueChanges
      .subscribe(password => {
        if(password == this.userForm.formGroup.get('password_confirmation').value) return this.userForm.passwordsMatch = true;
        return this.userForm.passwordsMatch = false;
      });

    this.passwordConfirmationSubscription = this.userForm.formGroup.controls['password_confirmation'].valueChanges
      .subscribe(passwordConfirmation => {
        if(passwordConfirmation == this.userForm.formGroup.get('password').value) return this.userForm.passwordsMatch = true;
        return this.userForm.passwordsMatch = false;
      });
  }

  ngOnDestroy() {
    this.emailSubscription.unsubscribe();
    this.passwordSubscription.unsubscribe();
    this.passwordConfirmationSubscription.unsubscribe();
  }

  alertForErrors() {
    const dialog = {
        title: 'Heads up!',
        message: 'Please check form for errors.',
        okay: 'Got it!'
    }

    this.alert.show(dialog);
  }

  checkDuplicate(email) {
    return this.dataService.checkDuplicate({ email });
  }

  submit() {
    if(this.userForm.formGroup.valid && this.userForm.passwordsMatch && !this.userForm.hasDuplicate)
    {
      return this.userForm.submit();
    }

    return this.alertForErrors();
  }
}
