import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

@Injectable()
export class UserFormService {
  formGroup: FormGroup;
  submitted: boolean;
  passwordsMatch: boolean;
  hasDuplicate: boolean;
  busy: boolean;

  private form = new Subject<any>();
  
  form$ = this.form.asObservable();

  constructor(private dialog: MdDialog, private fb: FormBuilder) { 
    this.formGroup = this.fb.group({
      'name': ['', [Validators.required]],
      'email': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required, Validators.minLength(8)]],
      'password_confirmation': ['', [Validators.required, Validators.minLength(8)]],
      'roles': this.fb.array([])
    });
  }

  confirmDiscardChanges() : Observable<boolean> | boolean {
    if(this.formGroup.dirty && !this.submitted) {
      return this.dialog.open(ConfirmationDialogComponent, {
        data: {
          title: 'Discard changes',
          message: 'Do you want to discard changes?',
          confirm: 'Discard',
          cancel: 'Cancel'
        }
      })
      .afterClosed()
      .map((resp: boolean) => {
        return resp;
      });
    }

    return true;
  }

  addRole() {
    const control = <FormArray>this.formGroup.controls['roles'];
    control.push(this.fb.group({'role': []}));
  }

  submit() {
    this.form.next();
  }
}
