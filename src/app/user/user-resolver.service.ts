import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { User } from '../models/user';
import { UserDataService } from './user-data.service';

@Injectable()
export class UserResolverService {

  constructor(
    private dataService: UserDataService,
    private router: Router,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<User> {
    let id = parseInt(route.paramMap.get('userId'));
    
    return this.dataService.show(id)
      .map(resp => {
        if(resp) return resp;
        this.router.navigate(['/page-not-found']);
        return null;
      })
      .catch(error => {
        this.router.navigate(['/page-not-found']);
        return Observable.throw(error);
      });
  }
}
