import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ApiService } from '../core/api.service';
import { Role } from '../models/role';

const url = 'role';

@Injectable()
export class RoleDataService {

  constructor(private api: ApiService) { }

  index(): Observable<Role[]> {
    return this.api.get(`${url}`);
  }

}
