import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from '../core/can-deactivate.guard';
import { CreateUserComponent } from './user-form/create-user/create-user.component';
import { EditUserComponent } from './user-form/edit-user/edit-user.component';
import { UserComponent } from './user.component';
import { RolesResolverService } from './roles-resolver.service';
import { UserResolverService } from './user-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: UserComponent
  },
  {
    path:'create',
    component: CreateUserComponent,
    canDeactivate: [CanDeactivateGuard],
    resolve: {
      roles: RolesResolverService,
    }
  },
  {
    path:':userId/edit',
    component: EditUserComponent,
    canDeactivate: [CanDeactivateGuard],
    resolve: {
      roles: RolesResolverService,
      user: UserResolverService,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RolesResolverService, UserResolverService]
})
export class UserRoutingModule { }
