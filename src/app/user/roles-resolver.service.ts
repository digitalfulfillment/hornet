import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Role } from '../models/role';
import { RoleDataService } from './role-data.service';
import { SpinnerService } from '../core/spinner/spinner.service';

@Injectable()
export class RolesResolverService {

  constructor(
    private dataService: RoleDataService,
    private router: Router,
    private spinner: SpinnerService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<Role> {
    this.spinner.show();
    
    return this.dataService.index()
      .map(resp => {
        this.spinner.hide();
        if(resp) return resp;
        this.router.navigate(['/page-not-found']);
        return null;
      })
      .catch(error => {
        this.spinner.hide();
        this.router.navigate(['/page-not-found']);
        return Observable.throw(error);
      });
  }
}
