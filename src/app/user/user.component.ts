import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { ExceptionService } from '../core/exception.service';
import { PushNotificationService } from '../core/push-notification.service';
import { SpinnerService } from '../core/spinner/spinner.service';
import { User } from '../models/user';
import { UserDataService } from './user-data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  loading: boolean;
  perPage: number;
  searchField: FormControl;
  searchFieldSubscription: Subscription;
  total: number;
  users: User[];

  constructor(
    private dataService: UserDataService, 
    private dialog: MdDialog,
    private exception: ExceptionService,
    private pushNotification: PushNotificationService,
    private spinner: SpinnerService,
  ) { 
    this.searchField = new FormControl();
  }

  ngOnInit() {
    this.searchFieldSubscription = this.searchField.valueChanges.debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => {
        this.loading = true;
        return this.search(term).catch(error => {
          this.loading = false;
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(resp => {
        this.loading = false;
        this.setData(resp);
      });

    this.initData();
  }

  delete(user: User) {
    this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Delete User',
        message: 'Are you sure you want to delete this user account?',
        confirm: 'Delete',
        cancel: 'Cancel'
      }
    }).afterClosed().subscribe(resp => {
      if(resp) this.deleteUser(user);
    })
  }

  deleteUser(user: User) {
    this.spinner.show('Removing');

    this.dataService.delete(user.id).subscribe(
      resp => this.users.splice(this.users.indexOf(user)),
      error => {
        this.spinner.hide();
        this.exception.takeAction(error);
      },
      () => {
        this.spinner.hide();
        this.pushNotification.simple('User deleted.');
      }
    )
  }

  initData() {
    this.loading = true;

    this.dataService.paginate().subscribe(
      resp =>  this.setData(resp),
      error => {
        this.loading = false;
        this.exception.takeAction(error);
      },
      () => this.loading = false
    );
  }

  search(term: string) {
    const query = {
      name: term,
      email: term
    }

    return this.dataService.search(query)
  }

  setData(resp) {
    this.perPage = resp.per_page;
    this.total = resp.total;
    this.users = resp.data;
  }
}
