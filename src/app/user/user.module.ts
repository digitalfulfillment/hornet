import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule, MdCardModule, MdCheckboxModule, MdDialogModule, MdListModule, MdInputModule, MdIconModule, MdMenuModule, MdProgressSpinnerModule, MdPaginatorModule, MdToolbarModule, MdTooltipModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserDataService } from './user-data.service';
import { UserFormComponent } from './user-form/user-form.component';
import { CreateUserComponent } from './user-form/create-user/create-user.component';
import { EditUserComponent } from './user-form/edit-user/edit-user.component';
import { RoleDataService } from './role-data.service';
import { UserFormService } from './user-form/user-form.service';

@NgModule({
  imports: [
    FormsModule,
    MdListModule,
    MdButtonModule,
    MdCardModule,
    MdCheckboxModule,
    MdDialogModule,
    MdListModule,
    MdInputModule,
    MdIconModule,
    MdMenuModule,
    MdProgressSpinnerModule,
    MdPaginatorModule,
    MdToolbarModule,
    MdTooltipModule,
    ReactiveFormsModule,
    SharedModule,
    UserRoutingModule
  ],
  declarations: [UserComponent, UserFormComponent, CreateUserComponent, EditUserComponent],
  providers: [RoleDataService, UserDataService]
})
export class UserModule { }
