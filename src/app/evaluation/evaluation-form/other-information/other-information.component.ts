import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import { EvaluationFormService } from '../evaluation-form.service';

@Component({
  selector: 'app-other-information',
  templateUrl: './other-information.component.html',
  styleUrls: ['./other-information.component.scss']
})
export class OtherInformationComponent implements OnInit, OnDestroy {
  @Input('formService') form: EvaluationFormService;

  formSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    this.formSubscription = this.form.getFormStatus().subscribe(resp => {
      if(!resp) {
        let untouchedForms = this.form.formGroup.controls.callDetails.controls.filter(group => {
          return group.controls['label'].untouched || group.controls['value'].untouched;
        });

        untouchedForms.forEach(group => {
          group.controls['label'].markAsTouched();
          group.controls['value'].markAsTouched();
        });
      }
    })
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }
}
