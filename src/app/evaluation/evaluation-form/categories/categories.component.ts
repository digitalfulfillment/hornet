import { Component, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { EvaluationFormService } from '../evaluation-form.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent {
  @Input('formService') form: EvaluationFormService
}
