import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Category } from '../../models/category';
import { ConfirmationDialog } from '../../interfaces/confirmation-dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { Evaluation } from '../../models/evaluation';
import { Questionnaire } from '../../models/questionnaire';
import { Question } from '../../models/question';

@Injectable()
export class EvaluationFormService {
  formGroup;
  private formStatus: Subject<boolean> = new Subject();

  constructor(
    private fb: FormBuilder,
    private d: MdDialog,
  ) { }

  sendformStatus(status: boolean) {
    this.formStatus.next(status);
  }

  getFormStatus(): Observable<boolean> {
    return this.formStatus.asObservable();
  }

  confirmRemove(data: ConfirmationDialog) : MdDialogRef<ConfirmationDialogComponent> {
      return this.d.open(ConfirmationDialogComponent, { data });
  }

  buildForm() {
    this.formGroup = this.fb.group({
      'agent': ['', [Validators.required]],
      'campaign': ['', [Validators.required]],
      'team': ['', [Validators.required]],
      'workQueue': ['', [Validators.required]],
      'callDate': [new Date(), [Validators.required]],
      'callTime': ['', [Validators.required]],
      'referenceCode': ['', [Validators.required]],
      'autoFail': [false, [Validators.required,]],
      'score': [0, []],
      'total_points': [0, []],
      'total_items': [0, []],
      'callDetails': this.fb.array([]), 
      'content': this.fb.array([]),
      'remarks': [, []],
    });
  }

  initCategory(): FormGroup {
    return this.fb.group({
      'name': ['', [Validators.required]],
      'questions': this.fb.array([])
    });
  }

  initQuestion(): FormGroup {
    return this.fb.group({
      'question': ['', Validators.required],
      'points': ['', [
        Validators.required,
        Validators.pattern('^[0-9]+$')
      ]],
      'score': [0, [
        Validators.required,
        Validators.pattern('^[0-9]+$')
      ]],
      'applicable': [true, [
        Validators.required,
      ]],
      'remarks': ['', []],
    });
  }

  initDetail() : FormGroup {
    return this.fb.group({
      'label': ['', Validators.required],
      'value': ['', Validators.required] 
    });
  }

  addCallDetail() {
    const control = <FormArray>this.formGroup.controls['callDetails'];
    control.push(this.initDetail());
  }

  addCategory() {
    const control = <FormArray>this.formGroup.controls['content'];
    control.push(this.initCategory());
  }
  
  addQuestion(i: number) {
    const control = this.formGroup.controls.content.controls[i].controls.questions;
    control.push(this.initQuestion());
  }

  removeCallDetail(i: number) {
    let dialog = {
      title: 'Remove Call Detail',
      message: 'Do you want to remove this call detail?',
      confirm: 'Remove',
      cancel: 'Cancel'
    }

    this.confirmRemove(dialog)
      .afterClosed()
      .subscribe(resp => {
        if(resp) {
            const control = <FormArray>this.formGroup.controls['callDetails'];
            control.removeAt(i);
        }
      })
  }

  setForm(questionnaire: Questionnaire) {
    questionnaire.content.forEach((category: Category, categoryIndex: number) => {
      this.addCategory();
      this.setFormQuestions(category, categoryIndex);
    });
  }

  setOtherInformation(evaluation: Evaluation) {
    evaluation.call_details.forEach((callDetail, callDetailIndex) => this.addCallDetail());
  }

  setFormQuestions(category: Category, categoryIndex: number) {
    category.questions.forEach((question, questionIndex) => this.addQuestion(categoryIndex));
  }

  subtotalItems(questions: Question[]): number {
    return questions
      .map(question => { 
        if(question.applicable) return +question.points;
        return 0;
      })
      .reduce((sum, value) => sum + value);
    }
    
  subtotalPoints(questions: Question[]): number {
    return questions
      .map(question => { 
        if(question.applicable) return +question.score; 
        return 0;
      })
      .reduce((sum, value) => sum + value);
  }

  computeScore(categories: Category[]) : number {
    this.formGroup.get('total_points').patchValue(this.totalPoints(categories));
    this.formGroup.get('total_items').patchValue(this.totalItems(categories));
    
    return +(this.formGroup.get('total_points').value / this.formGroup.get('total_items').value * 100).toFixed(2);
  }

  totalItems(categories: Category[]) : number {
    return categories
      .map(category => this.subtotalItems(category.questions))
      .reduce((sum, value) => sum + value);
  }

  totalPoints(categories: Category[]) : number {
    return categories
      .map(category => this.subtotalPoints(category.questions))
      .reduce((sum, value) => sum + value);
  }
}