import { Component, Input, OnInit, OnDestroy} from '@angular/core';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Agent } from '../../../models/agent';
import { AgentsDataService } from '../../../settings/agents/agents-data.service';
import { Campaign } from '../../../models/campaign';
import { CampaignsDataService } from '../../../settings/campaigns/campaigns-data.service';
import { Evaluation } from '../../../models/evaluation';
import { EvaluationDataService } from '../../evaluation-data.service';
import { EvaluationFormService } from '../evaluation-form.service';
import { ExceptionService } from '../../../core/exception.service';
import { Team } from '../../../models/team';
import { TeamsDataService } from '../../../settings/teams/teams-data.service';
import { WorkQueue } from '../../../models/work-queue';
import { WorkQueuesDataService } from '../../../settings/work-queues/work-queues-data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {
  @Input('formService') form: EvaluationFormService;
  @Input('evaluation') evaluation: Evaluation;
  @Input('submitted') submitted: boolean;
  
  agents: Agent[];
  campaigns: Campaign[];
  agentExists: boolean;
  campaignExists: boolean;
  teamExists: boolean;
  teams: Team[];
  workQueues: WorkQueue[];
  workQueueExists: boolean;
  hasDuplicate: boolean;

  agentControlSubscription: Subscription;
  campaignControlSubscription: Subscription;
  formSubscription: Subscription;
  teamControlSubscription: Subscription;
  workQueueControlSubscription: Subscription;
  referenceCodeSubscription: Subscription;

  constructor(
    private agentsData: AgentsDataService,
    private campaignsData: CampaignsDataService,
    private dataService: EvaluationDataService,
    private exception: ExceptionService,
    private teamsData: TeamsDataService,
    private workQueuesData: WorkQueuesDataService,
  ) { }

  ngOnInit() {
    this.agentControlSubscription = this.form.formGroup.get('agent').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()  
      .switchMap((resp) => { 
        if(typeof(resp) === 'string') {
          return this.agentsData.search({ 'name': resp}).catch(error => {
            this.exception.takeAction(error);
            return Observable.empty();
          });
        };

        return this.agentsData.search({ 'name': resp.name}).catch(error => {
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(resp => {
        if(resp.total) this.agents = resp.data;
        this.agentExists = typeof(this.form.formGroup.get('agent').value) === 'object' ? true : false;
      });

    this.campaignControlSubscription = this.form.formGroup.get('campaign').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()  
      .switchMap((resp) => { 
        if(typeof(resp) === 'string') {
          return this.campaignsData.search({ 'name': resp}).catch(error => {
            this.exception.takeAction(error);
            return Observable.empty();
          });
        };

        return this.campaignsData.search({ 'name': resp.name}).catch(error => {
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(resp => {
        if(resp.total) this.campaigns = resp.data;
        this.campaignExists = typeof(this.form.formGroup.get('campaign').value) === 'object' ? true : false;
      });

    this.teamControlSubscription = this.form.formGroup.get('team').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()  
      .switchMap((resp) => { 
        if(typeof(resp) === 'string') {
          return this.teamsData.search({ 'name': resp}).catch(error => {
            this.exception.takeAction(error);
            return Observable.empty();
          })
        };

        return this.teamsData.search({ 'name': resp.name}).catch(error => {
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(resp => {
        if(resp.total) this.teams = resp.data;
        this.teamExists = typeof(this.form.formGroup.get('team').value) === 'object' ? true : false;
      });

    this.workQueueControlSubscription = this.form.formGroup.get('workQueue').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()  
      .switchMap((resp) => { 
        if(typeof(resp) === 'string') {
          return this.workQueuesData.search({ 'name': resp}).catch(error => {
            this.exception.takeAction(error);
            return Observable.empty();
          })
        };

        if(this.form.formGroup.get('referenceCode').value && this.form.formGroup.get('workQueue').value)
        {
          this.checkDuplicate(this.form.formGroup.get('referenceCode').value)
            .subscribe(resp => this.hasDuplicate = resp ? true: false)
        }
          
        return this.workQueuesData.search({ 'name': resp.name}).catch(error => {
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(resp => {
        if(resp.total) this.workQueues = resp.data;
        this.workQueueExists = typeof(this.form.formGroup.get('workQueue').value) === 'object' ? true : false;
      });

    this.referenceCodeSubscription = this.form.formGroup.get('referenceCode').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(referenceCode => { 
        if(this.form.formGroup.get('referenceCode').value && this.form.formGroup.get('workQueue').value)
        {
          return this.checkDuplicate(referenceCode).catch(error => Observable.empty());
        }

        return Observable.empty();
      })
    .subscribe(resp => this.hasDuplicate = resp ? true: false);

    this.formSubscription = this.form.getFormStatus().subscribe(resp => {
      if(!resp) {
        this.form.formGroup.controls['referenceCode'].markAsTouched();
        this.form.formGroup.controls['agent'].markAsTouched();
        this.form.formGroup.controls['campaign'].markAsTouched();
        this.form.formGroup.controls['team'].markAsTouched();
        this.form.formGroup.controls['workQueue'].markAsTouched();
        this.form.formGroup.controls['callDate'].markAsTouched();
        this.form.formGroup.controls['callTime'].markAsTouched();
      }
    });

    this.initData();
  }

  ngOnDestroy() {
    this.agentControlSubscription.unsubscribe();
    this.campaignControlSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.teamControlSubscription.unsubscribe();
    this.workQueueControlSubscription.unsubscribe();
    this.referenceCodeSubscription.unsubscribe();
  }

  checkDuplicate(referenceCode: string) {
    const query = { 
      id: this.evaluation ? this.evaluation.id : null,
      reference_code: referenceCode,
      work_queue_id: this.form.formGroup.get('workQueue').value.id
    }

    return this.dataService.checkDuplicate(query);
  }

  displayName(model: { name:string }): string {
    return model.name;
  }

  initData() {
    this.agentsData.paginate().subscribe(resp => this.agents = resp.data);
    this.campaignsData.paginate().subscribe(resp => this.campaigns = resp.data);
    this.teamsData.paginate().subscribe(resp => this.teams = resp.data);
    this.workQueuesData.paginate().subscribe(resp => this.workQueues = resp.data);
  }
}
