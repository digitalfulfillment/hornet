import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { AlertDialogService } from '../../core/alert-dialog/alert-dialog.service';
import { Category } from '../../models/category';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { Evaluation } from '../../models/evaluation';
import { EvaluationDataService } from '../evaluation-data.service';
import { EvaluationFormService } from './evaluation-form.service';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { Question } from '../../models/question';
import { Questionnaire } from '../../models/questionnaire';
import { SpinnerService } from '../../core/spinner/spinner.service';

@Component({
  selector: 'app-evaluation-form',
  templateUrl: './evaluation-form.component.html',
  styleUrls: ['./evaluation-form.component.scss'],
})
export class EvaluationFormComponent implements OnInit {
  questionnaire: Questionnaire;
  submitted: boolean;
  saved: boolean;
  isFormValid: boolean;

  formSubscription: Subscription;

  constructor(
    protected alertDialog: AlertDialogService,
    protected activatedRoute: ActivatedRoute,
    protected dataService: EvaluationDataService,
    protected dialog: MdDialog,
    public form: EvaluationFormService,
    protected exception: ExceptionService,
    protected pushNotification: PushNotificationService,
    protected router: Router,
    protected spinner: SpinnerService,
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: { questionnaire: Questionnaire}) => { 
      if(data.questionnaire)
      {
        this.questionnaire = data.questionnaire;
        this.form.buildForm();
        this.form.setForm(this.questionnaire);
        this.form.formGroup.get('content').patchValue(this.questionnaire.content);
      }
    });

    this.formSubscription = this.form.formGroup.get('content').valueChanges.subscribe((resp: Category[]) => { 
      this.form.formGroup.get('score').patchValue(this.form.computeScore(resp));
    });
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  canDeactivate() : Observable<boolean> | boolean {
      return this.confirmDiscardChanges();
  }

  confirmDiscardChanges() {
    if(this.form.formGroup.dirty && !this.saved) {
      return this.dialog.open(ConfirmationDialogComponent, {
        data: {
          title: 'Discard changes',
          message: 'Do you want to discard changes?',
          confirm: 'Discard',
          cancel: 'Cancel'
        }
      })
      .afterClosed()
      .map((resp: boolean) => {
        return resp;
      });
    }

    return true;
  }

  displayName(model: { name:string }): string {
    return model.name;
  }

  protected prepareEvaluation() : Evaluation {
    return new Evaluation({
      agent_id: this.form.formGroup.get('agent').value.id,
      campaign_id: this.form.formGroup.get('campaign').value.id,
      team_id: this.form.formGroup.get('team').value.id,
      work_queue_id: this.form.formGroup.get('workQueue').value.id,
      reference_code: this.form.formGroup.get('referenceCode').value,
      auto_fail: this.form.formGroup.get('autoFail').value,
      call_history: this.setCallHistory(),
      call_details: this.form.formGroup.get('callDetails').value,
      content: this.form.formGroup.get('content').value,
      remarks: this.form.formGroup.get('remarks').value
    });
  }

  protected setCallHistory(): string {
    let callHistory = new Date(this.form.formGroup.get('callDate').value);

    callHistory.setHours(
      this.form.formGroup.get('callTime').value.hour,
      this.form.formGroup.get('callTime').value.minute,
      this.form.formGroup.get('callTime').value.second,
    )

    return `${callHistory.toDateString()} ${callHistory.toLocaleTimeString()}`;
  }

  save() {
    this.dataService.store(this.prepareEvaluation())
    .subscribe(
      () => { 
        this.spinner.hide();
        this.pushNotification.simple('Evaluation created.');
        this.saved = true;
      },
      error => { 
        this.spinner.hide();
        this.exception.takeAction(error);
      },
      () => this.router.navigate(['/evaluation'])
    );
  }

  submit() {
    this.submitted = true;

    if(this.form.formGroup.valid) {
      return this.dialog.open(ConfirmationDialogComponent, {
        data: {
          title: 'Submit form',
          message: `When you submit the form it can't be undone`,
          confirm: 'Submit',
          cancel: 'Cancel',
        }
      }).afterClosed().subscribe(resp => {
        if(resp) {
          this.spinner.show();
          this.save();
        }
      });
    }

    this.alertDialog.show({
      title: 'Heads up!',
      message: 'Please check form for errors.',
      okay: 'Got it!'
    });

    return this.form.sendformStatus(false);
  }
}
