import { TestBed, async, inject } from '@angular/core/testing';

import { UpdateEvaluationGuard } from './update-evaluation.guard';

describe('UpdateEvaluationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdateEvaluationGuard]
    });
  });

  it('should ...', inject([UpdateEvaluationGuard], (guard: UpdateEvaluationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
