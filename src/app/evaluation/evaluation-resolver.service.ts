import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Evaluation } from '../models/evaluation';
import { EvaluationDataService } from './evaluation-data.service';
import { SpinnerService } from '../core/spinner/spinner.service';

@Injectable()
export class EvaluationResolverService {

  constructor(
    private dataService: EvaluationDataService,
    private router: Router,
    private spinner: SpinnerService,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state:RouterStateSnapshot): Observable<Evaluation> {
    let id = parseInt(route.paramMap.get('evaluationId'));

    this.spinner.show();
    
    return this.dataService.show(id)
      .map(resp => {
        this.spinner.hide();
        if(resp) return resp;
        this.router.navigate(['/page-not-found']);
        return null;
      })
      .catch(error => {
        this.spinner.hide();
        this.router.navigate(['/page-not-found']);
        return Observable.throw(error);
      });
  }
}
