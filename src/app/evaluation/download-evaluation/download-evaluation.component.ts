import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

import { EvaluationSearchService } from '../evaluation-search.service';
import { EvaluationDataService } from '../evaluation-data.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-download-evaluation',
  templateUrl: './download-evaluation.component.html',
  styleUrls: ['./download-evaluation.component.scss'],
  providers: [EvaluationSearchService]
})
export class DownloadEvaluationComponent implements OnInit {
  busy: boolean;

  constructor(
    private dataService:EvaluationDataService, 
    public form: EvaluationSearchService,
    private dialog: MdDialogRef<DownloadEvaluationComponent>,
  ) { }

  ngOnInit() {
  }

  submit() {
    if(this.form.formGroup.valid)
    {
      const from = this.form.formGroup.get('from').value.toDateString();
      const to = this.form.formGroup.get('to').value.toDateString();

      window.open(
        `${environment.apiUrl}/evaluation/overall/${from}/to/${to}`, 
        '_blank'
      );
      this.dialog.close();
    }
  }
}
