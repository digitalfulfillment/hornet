import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadEvaluationComponent } from './download-evaluation.component';

describe('DownloadEvaluationComponent', () => {
  let component: DownloadEvaluationComponent;
  let fixture: ComponentFixture<DownloadEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadEvaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
