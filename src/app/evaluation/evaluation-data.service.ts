import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { ApiService } from '../core/api.service';
import { Evaluation } from '../models/evaluation';
import { Paginated } from '../interfaces/paginated';

const url = 'evaluation';

@Injectable()
export class EvaluationDataService {

  constructor(private api: ApiService) { }

  create(): Observable<boolean> {
    return this.api.get(`${url}/create`);
  }

  checkDuplicate(query) {
    return this.api.post(`${url}/check-duplicate`, query);
  }

  delete(id: number): Observable<void> {
    return this.api.delete(`${url}/${id}`);
  }

  edit(id: number): Observable<boolean> {
    return this.api.get(`${url}/${id}/edit`);
  }

  paginate(page?: number): Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`).map(resp => this.setDates(resp));
  }

  setDates(paginated: Paginated): Paginated {
    paginated.data.forEach(evaluation => {
      evaluation.call_history = new Date(evaluation.call_history);
      evaluation.created_at = new Date(evaluation.created_at);
      evaluation.deleted_at = new Date(evaluation.deleted_at);
    });

    return paginated;
  }

  search(query: object, page? :number): Observable<Paginated> {
    return this.api.post(`${url}/search?page=${page}`, query).map(resp => this.setDates(resp));
  }

  show(id: number) : Observable<Evaluation> {
    return this.api.get(`${url}/${id}`);
  }
  
  store(evaluation: Evaluation): Observable<void> {
    return this.api.post(`${url}`, evaluation);
  }

  update(evaluation: Evaluation) {
    return this.api.put(`${url}/${evaluation.id}`, evaluation);
  }
}
