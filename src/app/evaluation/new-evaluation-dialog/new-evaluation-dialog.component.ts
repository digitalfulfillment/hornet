import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Questionnaire } from '../../models/questionnaire';
import { QuestionnaireDataService } from '../../questionnaire/questionnaire-data.service';

@Component({
  selector: 'app-new-evaluation-dialog',
  templateUrl: './new-evaluation-dialog.component.html',
  styleUrls: ['./new-evaluation-dialog.component.scss']
})
export class NewEvaluationDialogComponent implements OnInit {
  busy: boolean;
  questionnaireFormControl: FormControl
  questionnaires: Questionnaire[] = [];
  questionnaireExists: boolean;
  formSubscription: Subscription;

  constructor(
    private dataService: QuestionnaireDataService,
    private dialogRef: MdDialogRef<NewEvaluationDialogComponent>,
    private router: Router,
  ) { }

  ngOnInit() {
    this.questionnaireFormControl = new FormControl('', [
      Validators.required,
    ]);

    this.formSubscription = this.questionnaireFormControl.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(resp => {
        if(typeof(resp) === 'string') return this.dataService.search({ 'name': resp}).catch(error => {
          this.dialogRef.close({ error });
          return Observable.empty()
        });
        return this.dataService.search({ 'name': resp.name}).catch(error => {
          this.dialogRef.close({ error });
          return Observable.empty()
        });
      })
      .subscribe((resp: any) => {
        if(resp.total) this.questionnaires = resp.data;
        this.questionnaireExists = typeof(this.questionnaireFormControl.value) === 'object' ? true : false;
      })

    this.dataService.paginate().subscribe(
      resp => this.questionnaires = resp.data,
      error => this.dialogRef.close({ error }),
    );
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  displayName(questionnaire: { name:string }): string {
    return questionnaire.name;
  }

  searchUserInput(name: string) {
    return this.dataService.search({ name });
  }

  submit() {
    if(this.questionnaireFormControl.valid && this.questionnaireExists){
      this.busy = true;
      this.router.navigate([`/evaluation/${this.questionnaireFormControl.value.id}/create`])
        .then(() => {
          this.busy = false;
          this.dialogRef.close();
        })
        .catch(() => {
          this.busy = false;
        })
    }
  }
}
