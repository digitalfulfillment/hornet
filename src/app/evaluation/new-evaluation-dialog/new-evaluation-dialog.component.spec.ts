import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEvaluationDialogComponent } from './new-evaluation-dialog.component';

describe('NewEvaluationDialogComponent', () => {
  let component: NewEvaluationDialogComponent;
  let fixture: ComponentFixture<NewEvaluationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEvaluationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEvaluationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
