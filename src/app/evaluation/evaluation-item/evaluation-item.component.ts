import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MdDialog } from '@angular/material';

import { AuthService } from '../../auth/auth.service';
import { Evaluation } from '../../models/evaluation';
import { EvaluationDataService } from '../evaluation-data.service';
import { ExceptionService } from '../../core/exception.service';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { PushNotificationService } from '../../core/push-notification.service';
import { SpinnerService } from '../../core/spinner/spinner.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-evaluation-item',
  templateUrl: './evaluation-item.component.html',
  styleUrls: ['./evaluation-item.component.scss']
})
export class EvaluationItemComponent implements OnInit {
  evaluation: Evaluation;
  canDelete: boolean;
  canEdit: boolean;
  details: any[] = [];

  constructor(
    private auth: AuthService,
    private activedRoute: ActivatedRoute,
    private dataService: EvaluationDataService,
    private dialog: MdDialog,
    private exception: ExceptionService,
    private pushNotification: PushNotificationService,
    private router: Router,
    private spinner: SpinnerService,
  ) { }

  ngOnInit() {
    this.activedRoute.data.subscribe((data: { evaluation: Evaluation }) => {
      this.evaluation = data.evaluation;
      this.setDetails(data.evaluation);
    });

    this.canDelete = this.auth.user.is_admin || this.auth.user.roles.filter(role => role.name == 'delete-reports').length ? true : false;
    this.canEdit = this.auth.user.id === this.evaluation.user_id;
  }

  deleteEvaluation() {
    this.spinner.show();
    this.dataService.delete(this.evaluation.id).subscribe(
      () => { 
        this.spinner.hide();
        this.pushNotification.simple('Evaluation deleted.');
      },
      error => {
        this.spinner.hide();
        this.exception.takeAction(error);
      },
      () => this.router.navigate(['/evaluation'])
    )
  }

  deleteDialog() {
    this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Delete Evaluation',
        message: `Once you delete this evaluation, it can't be undone.`,
        confirm: 'Delete',
        cancel: 'Cancel'
      }
    }).afterClosed().subscribe(resp => {
      if(resp) this.deleteEvaluation();
    })
  }

  download(id: number) {
    window.open(`${environment.apiUrl}/evaluation/score-card/${id}`, '_blank');
  }

  setDetails(evaluation: Evaluation) {
    this.details.push({ label: 'Employee Name', value: evaluation.agent.name});
    this.details.push({ label: 'Employee ID', value: evaluation.agent.employee_number});
    this.details.push({ label: 'Team', value: evaluation.team.name});
    this.details.push({ label: 'Work Queue', value: evaluation.work_queue.name});
    this.details.push({ label: 'Date and Time', value: `${new Date(evaluation.call_history).toLocaleString()}` });
    this.details.push({ label: 'Evaluated by', value: evaluation.user.name});
    this.details.push({ label: 'Evaluated at', value: `${new Date(evaluation.created_at).toLocaleString()}`});
    this.details.push({ label: 'Last updated', value: `${new Date(evaluation.updated_at).toLocaleString()}`});
  }
}
