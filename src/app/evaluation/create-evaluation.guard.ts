import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/observable/of';

import { EvaluationDataService } from './evaluation-data.service';

@Injectable()
export class CreateEvaluationGuard implements CanActivate {
  constructor(
    private dataService: EvaluationDataService,
    private router: Router,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canCreate();
  }

  canCreate() : Observable<boolean> {
    return this.dataService.create()
      .map((resp: boolean) => resp)
      .catch(error => {
        this.router.navigate(['/page-not-found']);
        return Observable.of(false)
      })
      .first();
  }
}
