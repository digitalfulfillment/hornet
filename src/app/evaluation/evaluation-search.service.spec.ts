import { TestBed, inject } from '@angular/core/testing';

import { EvaluationSearchService } from './evaluation-search.service';

describe('EvaluationSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvaluationSearchService]
    });
  });

  it('should be created', inject([EvaluationSearchService], (service: EvaluationSearchService) => {
    expect(service).toBeTruthy();
  }));
});
