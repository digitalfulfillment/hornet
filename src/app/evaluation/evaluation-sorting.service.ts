import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'; 
import { Subject } from 'rxjs/Subject';

import { Evaluation } from '../models/evaluation';
import { Sorting } from '../interfaces/sorting';

@Injectable()
export class EvaluationSortingService implements Sorting{
  sortType: string = 'created_at';
  sortReverse: boolean = false;
  sortedBy: string;
  sortHeaders: [{ label, value}] = [
    { label: 'Name', value: 'agent'},
    { label: 'Campaign', value: 'campaign'},
    { label: 'Team', value: 'team'},
    { label: 'Work Queue', value: 'work_queue'},
    { label: 'Reference ID', value: 'reference_code'},
    { label: 'Date and Time', value: 'call_history'},
    { label: 'Score', value: 'score'},
    { label: 'Auto Fail', value: 'auto_fail'},
    { label: 'Evaluated by', value: 'user'},
    { label: 'Evaluated at', value: 'created_at'},
    { label: 'Last Updated', value: 'updated_at'}
  ];
  private sortingSubject = new Subject<string>()

  sortCall$ = this.sortingSubject.asObservable();

  sortCall()
  {
    this.sortingSubject.next();
  }

  sort(evaluations: Evaluation[], key: string = this.sortType, order: boolean = this.sortReverse): Evaluation[] {
    this.sortType = key;
    this.sortReverse = order;
    this.sortedBy = this.sortHeaders.filter(item => item.value === key)[0].label;

    switch (key) {
      case 'reference_code' :
        if(!this.sortReverse){
          return evaluations.sort((a, b) => {
            let current = a[this.sortType].toUpperCase(); // ignore upper and lowercase
            let next = b[this.sortType].toUpperCase(); // ignore upper and lowercase
      
            if (current < next) {
              return -1;
            }
            if (current > next) {
              return 1;
            }
          
            return 0;
          });
        }

        return evaluations.sort((a, b) => {
          let current = a[this.sortType].toUpperCase(); // ignore upper and lowercase
          let next = b[this.sortType].toUpperCase(); // ignore upper and lowercase
    
          if (current < next) {
            return 1;
          }
          if (current > next) {
            return -1;
          }
        
          return 0;
        });
      case 'score':
        if(!this.sortReverse) return evaluations.sort((a, b) => a[this.sortType] - b[this.sortType]);  

        return evaluations.sort((a, b) => b[this.sortType] - a[this.sortType]);
      
      case 'auto_fail':
      case 'call_history':
      case 'created_at':
      case 'updated_at':
        if(!this.sortReverse)
        {
          return evaluations.sort((a, b) => {
            if (a[this.sortType] < b[this.sortType]) {
              return -1;
            }
            if (a[this.sortType] > b[this.sortType]) {
              return 1;
            }
          
            return 0;
          })
        } 

        return evaluations.sort((a, b) => {
          if (a[this.sortType] < b[this.sortType]) {
            return 1;
          }
          if (a[this.sortType] > b[this.sortType]) {
            return -1;
          }
        
          return 0;
        })

      default:
        if(!this.sortReverse) return this.ascendingByPropertyName(evaluations, this.sortType);
      
        return this.descendingByPropertyName(evaluations, this.sortType);
    }
  }

  ascendingByPropertyName(evaluations: Evaluation[], sortType: string): Evaluation[] {
    return evaluations.sort((a, b) => {
      let current = a[sortType].name.toUpperCase(); // ignore upper and lowercase
      let next = b[sortType].name.toUpperCase(); // ignore upper and lowercase

      if (current < next) {
        return -1;
      }
      if (current > next) {
        return 1;
      }
    
      return 0;
    });
  }

  descendingByPropertyName(evaluations: Evaluation[], sortType: string): Evaluation[] {
    return evaluations.sort((a, b) => {
      let current = a[sortType].name.toUpperCase(); // ignore upper and lowercase
      let next = b[sortType].name.toUpperCase(); // ignore upper and lowercase

      if (current < next) {
        return 1;
      }
      if (current > next) {
        return -1;
      }
    
      return 0;
    });
  }
}
