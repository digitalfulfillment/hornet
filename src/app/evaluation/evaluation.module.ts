import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdAutocompleteModule, MdButtonModule, MdCardModule, MdDatepickerModule, MdDialogModule, MdIconModule, MdInputModule, MdListModule, MdNativeDateModule, MdPaginatorModule, MdProgressSpinnerModule, MdRadioModule, MdSlideToggleModule, MdTableModule, MdToolbarModule, MdTooltipModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { EvaluationRoutingModule } from './evaluation-routing.module';
import { EvaluationFormComponent } from './evaluation-form/evaluation-form.component';
import { EvaluationFormService } from './evaluation-form/evaluation-form.service';
import { EvaluationComponent } from './evaluation.component';
import { OtherInformationComponent } from './evaluation-form/other-information/other-information.component';
import { ScoreCardComponent } from './evaluation-form/score-card/score-card.component';
import { DetailsComponent } from './evaluation-form/details/details.component';
import { CategoriesComponent } from './evaluation-form/categories/categories.component';
import { NewEvaluationDialogComponent } from './new-evaluation-dialog/new-evaluation-dialog.component';
import { EvaluationItemComponent } from './evaluation-item/evaluation-item.component';
import { RemarksComponent } from './evaluation-form/remarks/remarks.component';
import { EditEvaluationComponent } from './edit-evaluation/edit-evaluation.component';
import { DownloadEvaluationComponent } from './download-evaluation/download-evaluation.component';
import { EvaluationSortingService } from './evaluation-sorting.service';

@NgModule({
  imports: [
    FormsModule,
    MdAutocompleteModule,
    MdButtonModule,
    MdCardModule,
    MdDatepickerModule,
    MdDialogModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdNativeDateModule,
    MdPaginatorModule,
    MdProgressSpinnerModule,
    MdRadioModule,
    MdSlideToggleModule,
    MdTableModule,
    MdToolbarModule,
    MdTooltipModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    SharedModule,
    EvaluationRoutingModule
  ],
  declarations: [EvaluationFormComponent, EvaluationComponent, OtherInformationComponent, ScoreCardComponent, DetailsComponent, CategoriesComponent, NewEvaluationDialogComponent, EvaluationItemComponent, RemarksComponent, EditEvaluationComponent, DownloadEvaluationComponent],
  entryComponents: [DownloadEvaluationComponent, NewEvaluationDialogComponent],
  providers: [EvaluationFormService, EvaluationSortingService]
})
export class EvaluationModule { }
