import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/observable/of';

import { EvaluationDataService } from './evaluation-data.service';

@Injectable()
export class UpdateEvaluationGuard implements CanActivate {
  constructor(
    private dataService: EvaluationDataService,
    private router: Router,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canUpdate(next.paramMap.get('evaluationId'));
  }

  canUpdate(evaluationId) : Observable<boolean> {
    let id = parseInt(evaluationId);

    return this.dataService.edit(id)
      .map((resp: boolean) => resp)
      .catch(error => {
        this.router.navigate(['/page-not-found']);
        return Observable.of(false);
      })
      .first();
  }
}
