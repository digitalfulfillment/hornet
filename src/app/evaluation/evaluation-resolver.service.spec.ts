import { TestBed, inject } from '@angular/core/testing';

import { EvaluationResolverService } from './evaluation-resolver.service';

describe('EvaluationResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvaluationResolverService]
    });
  });

  it('should be created', inject([EvaluationResolverService], (service: EvaluationResolverService) => {
    expect(service).toBeTruthy();
  }));
});
