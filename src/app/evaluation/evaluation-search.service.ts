import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class EvaluationSearchService {
  formGroup: FormGroup;

  constructor(private fb: FormBuilder) { 
    this.buildForm();
  }

  buildForm() {
    this.formGroup = this.fb.group({
      searchField: ['', []],
      from: [this.getMonday(), [Validators.required]],
      to: [this.getSaturday(), [Validators.required]]
    });
  }

  getMonday() {
    let d = new Date();
    let day = d.getDay();
    let diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }

  getSaturday() {
    let monday = this.getMonday();
    return new Date(monday.setDate(monday.getDate() + 5));
  }
}