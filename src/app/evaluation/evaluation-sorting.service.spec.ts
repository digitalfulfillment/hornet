import { TestBed, inject } from '@angular/core/testing';

import { EvaluationSortingService } from './evaluation-sorting.service';

describe('EvaluationSortingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvaluationSortingService]
    });
  });

  it('should be created', inject([EvaluationSortingService], (service: EvaluationSortingService) => {
    expect(service).toBeTruthy();
  }));
});
