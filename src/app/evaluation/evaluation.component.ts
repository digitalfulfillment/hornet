import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { PageEvent } from '@angular/material'; 
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { AuthService } from '../auth/auth.service';
import { DownloadEvaluationComponent } from './download-evaluation/download-evaluation.component';
import { ExceptionService } from '../core/exception.service';
import { Evaluation } from '../models/evaluation';
import { EvaluationDataService } from './evaluation-data.service';
import { EvaluationSearchService } from './evaluation-search.service';
import { EvaluationSortingService } from './evaluation-sorting.service';
import { NewEvaluationDialogComponent } from './new-evaluation-dialog/new-evaluation-dialog.component';
import { Paginated } from '../interfaces/paginated';
import { EvaluationListService } from '../shared/evaluation-list/evaluation-list.service';
import { PusherService } from '../pusher.service';

@Component({
  selector: 'app-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss'],
  providers: [EvaluationSearchService]
})
export class EvaluationComponent implements OnInit, OnDestroy {
  canEvaluate: boolean;
  loading: boolean;
  evaluations: Evaluation[];
  evaluationSubscription: Subscription;
  pusherSubscription: Subscription;
  perPage: number;
  searchField: FormControl;
  total: number;

  constructor(
    private auth: AuthService,
    private dialog: MdDialog,
    private dataService: EvaluationDataService,
    private exception: ExceptionService,
    private evaluationList: EvaluationListService,
    public form: EvaluationSearchService,
    private pusher: PusherService,
    private router: Router,
    private sorting: EvaluationSortingService,
  ) { }

  ngOnInit() {
    this.canEvaluate = this.auth.user.is_admin || this.auth.user.roles.filter(role => role.name == 'manage-reports').length ? true : false;

    this.form.formGroup.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => {
        if(this.form.formGroup.invalid) return Observable.empty();
        
        this.loading = true;
        const query = this.searchQuery(this.form.formGroup.value);

        return this.searchUserInputs(query).catch(error => {
          this.loading = false;
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(
        resp => {
          this.loading = false;
          this.setData(resp);
          this.sorting.sortCall();
        },
      );
      
    this.evaluationSubscription = this.evaluationList.evaluation$.subscribe(evaluation => this.view(evaluation));
    this.pusherSubscription = this.pusher.newNotification$.subscribe(() => this.initData());

    this.initData();
  }

  ngOnDestroy() {
    this.evaluationSubscription.unsubscribe();
  }

  downloadDialog() {
    this.dialog.open(DownloadEvaluationComponent);
  }

  initData(): void {
    this.loading = true;

    const query = this.searchQuery(this.form.formGroup.value);
    
    this.searchUserInputs(query).subscribe(
      resp => this.setData(resp),
      error => this.exception.takeAction(error),
      () => this.loading = false
    );
  }

  newEvaluationDialog(): void {
    this.dialog.open(NewEvaluationDialogComponent).afterClosed().subscribe(resp => {
      if(resp) this.exception.takeAction(resp.error);
    });
  }

  pageEventCall(pageEvent: PageEvent) {
    this.loading = true;

    if(this.form.formGroup.valid)
    {
      const query = this.searchQuery(this.form.formGroup.value);

      return this.searchUserInputs(query, pageEvent.pageIndex + 1)
        .subscribe(
          resp => this.setData(resp),
          error => {
            this.loading = false;
            this.exception.takeAction(error)
          },
          () => {
            this.loading = false;
            this.sorting.sortCall();
          }
        )
    }
  }
  
  searchUserInputs(query, page?: number): Observable<Paginated> {
    return this.dataService.search(query, page);
  }

  searchQuery(data) {
    return {
      reference_code: data.searchField,
      call_history: {
        from: data.from.toDateString(),
        to: data.to.toDateString(),
      },
      created_at: {
        from: data.from.toDateString(),
        to: data.to.toDateString(),
      },
      updated_at: {
        from: data.from.toDateString(),
        to: data.to.toDateString(),
      },
      agent: {
        column: 'name',
        value: data.searchField
      },
      campaign: {
        column: 'name',
        value: data.searchField
      },
      team: {
        column: 'name',
        value: data.searchField
      },
      work_queue: {
        column: 'name',
        value: data.searchField
      },
    }
  }

  setData(data): void {
    this.total = data.total;
    this.perPage = data.per_page;
    this.evaluations = data.data;
  }

  view(evaluation: Evaluation) {
    this.router.navigate([`/evaluation/${evaluation.id}`]);
  }
}
