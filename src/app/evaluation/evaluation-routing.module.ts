import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from '../core/can-deactivate.guard';
import { EditEvaluationComponent } from './edit-evaluation/edit-evaluation.component';
import { EvaluationComponent } from './evaluation.component';
import { EvaluationFormComponent } from './evaluation-form/evaluation-form.component';
import { EvaluationItemComponent } from './evaluation-item/evaluation-item.component';
import { EvaluationResolverService } from './evaluation-resolver.service';
import { CreateEvaluationGuard } from './create-evaluation.guard';
import { QuestionnaireResolverService } from '../questionnaire/questionnaire-resolver.service';
import { UpdateEvaluationGuard } from './update-evaluation.guard';

const routes: Routes = [
  { 
    path:'',
    component:EvaluationComponent 
  },
  {
    path:':evaluationId',
    component: EvaluationItemComponent,
    resolve: {
      evaluation: EvaluationResolverService,
    }
  },
  {
    path:':questionnaireId/create',
    component: EvaluationFormComponent,
    resolve: {
      questionnaire: QuestionnaireResolverService
    },
    canActivate: [CreateEvaluationGuard],
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path:':evaluationId/edit',
    component: EditEvaluationComponent,
    resolve: {
      evaluation: EvaluationResolverService
    },
    canActivate: [UpdateEvaluationGuard],
    canDeactivate: [CanDeactivateGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [EvaluationResolverService, CreateEvaluationGuard, UpdateEvaluationGuard]
})
export class EvaluationRoutingModule { }
