import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MdDialog } from '@angular/material';

import { AlertDialogService } from '../../core/alert-dialog/alert-dialog.service';
import { Category } from '../../models/category';
import { Evaluation } from '../../models/evaluation';
import { EvaluationDataService } from '../evaluation-data.service';
import { EvaluationFormComponent } from '../evaluation-form/evaluation-form.component';
import { EvaluationFormService } from '../evaluation-form/evaluation-form.service';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { SpinnerService } from '../../core/spinner/spinner.service';
import { Questionnaire } from "../../models/questionnaire";

@Component({
  selector: 'app-edit-evaluation',
  templateUrl: './edit-evaluation.component.html',
  styleUrls: ['./edit-evaluation.component.scss']
})
export class EditEvaluationComponent extends EvaluationFormComponent implements OnInit, OnDestroy {
  evaluation: Evaluation;

  constructor(
    protected alertDialog: AlertDialogService,
    protected activatedRoute: ActivatedRoute,
    protected dataService: EvaluationDataService,
    protected dialog: MdDialog,
    public form: EvaluationFormService,
    protected exception: ExceptionService,
    protected pushNotification: PushNotificationService,
    protected router: Router,
    protected spinner: SpinnerService,
  ) {
    super(
      alertDialog,
      activatedRoute,
      dataService,
      dialog,
      form,
      exception,
      pushNotification,
      router,
      spinner,
    );
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: { evaluation: Evaluation}) => { 
      this.evaluation = data.evaluation;

      this.questionnaire = new Questionnaire({
        name: 'new',
        content: this.evaluation.content
      });

      this.form.buildForm();
      this.form.setForm(this.questionnaire);
      this.form.setOtherInformation(this.evaluation);
      this.populateForm();
    });

    this.formSubscription = this.form.formGroup.get('content').valueChanges.subscribe((resp: Category[]) => { 
      this.form.formGroup.get('score').patchValue(this.form.computeScore(resp));
    });
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  populateForm() {
    let callHistory = new Date(this.evaluation.call_history);
    this.form.formGroup.patchValue(this.evaluation);
    this.form.formGroup.get('referenceCode').patchValue(this.evaluation.reference_code);
    this.form.formGroup.get('autoFail').patchValue(this.evaluation.auto_fail);
    this.form.formGroup.get('workQueue').patchValue(this.evaluation.work_queue);
    this.form.formGroup.get('callDate').patchValue(callHistory);
    this.form.formGroup.get('callTime').patchValue({hour: callHistory.getHours(), minute: callHistory.getMinutes(), second:0 });
    this.form.formGroup.get('callDetails').patchValue(this.evaluation.call_details);
  }

  save() {
    let evaluation = this.prepareEvaluation();
    evaluation.id = this.evaluation.id;

    this.dataService.update(evaluation)
      .subscribe(
        () => { 
          this.spinner.hide();
          this.pushNotification.simple('Changes saved.');
          this.saved = true;
        },
        error => { 
          this.spinner.hide();
          this.exception.takeAction(error);
        },
        () => this.router.navigate(['/evaluation'])
      );
  }
}
