import { TestBed, async, inject } from '@angular/core/testing';

import { CreateEvaluationGuard } from './create-evaluation.guard';

describe('CreateEvaluationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CreateEvaluationGuard]
    });
  });

  it('should ...', inject([CreateEvaluationGuard], (guard: CreateEvaluationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
