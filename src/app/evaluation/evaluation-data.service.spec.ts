import { TestBed, inject } from '@angular/core/testing';

import { EvaluationDataService } from './evaluation-data.service';

describe('EvaluationDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvaluationDataService]
    });
  });

  it('should be created', inject([EvaluationDataService], (service: EvaluationDataService) => {
    expect(service).toBeTruthy();
  }));
});
