import { NgModule } from '@angular/core';
import { MdButtonModule, MdDialogModule, MdSnackBarModule, MdProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { SharedModule } from './../shared/shared.module';
import { FormValidationMessageService } from './form-validation-message.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ExceptionService } from './exception.service';
import { ExceptionDialogComponent } from './exception-dialog/exception-dialog.component';
import { ApiService } from './api.service';
import { PushNotificationService } from './push-notification.service';
import { SpinnerComponent } from './spinner/spinner.component';
import { SpinnerService } from './spinner/spinner.service';
import { CanDeactivateGuard } from './can-deactivate.guard';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { AlertDialogService } from './alert-dialog/alert-dialog.service';
import { QuestionnaireDataService } from '../questionnaire/questionnaire-data.service';
import { EvaluationDataService } from '../evaluation/evaluation-data.service';
import { AgentsDataService } from '../settings/agents/agents-data.service';
import { CampaignsDataService } from '../settings/campaigns/campaigns-data.service';
import { TeamsDataService } from '../settings/teams/teams-data.service';
import { WorkQueuesDataService } from '../settings/work-queues/work-queues-data.service';
import { QuestionnaireResolverService } from '../questionnaire/questionnaire-resolver.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    MdButtonModule,
    MdDialogModule,
    MdProgressSpinnerModule,
    MdSnackBarModule,
    NoopAnimationsModule,
    SharedModule,
  ],
  declarations: [PageNotFoundComponent, ExceptionDialogComponent, SpinnerComponent, AlertDialogComponent],
  entryComponents: [AlertDialogComponent, ExceptionDialogComponent, SpinnerComponent],
  providers: [
    FormValidationMessageService,
    ExceptionService, 
    ApiService, 
    PushNotificationService, 
    SpinnerService, 
    CanDeactivateGuard, 
    AlertDialogService, 
    QuestionnaireDataService,
    EvaluationDataService,
    AgentsDataService,
    CampaignsDataService,
    TeamsDataService,
    WorkQueuesDataService,
    QuestionnaireResolverService,
  ],
})
export class CoreModule { }
