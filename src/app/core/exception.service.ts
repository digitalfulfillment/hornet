import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { Exception } from './exception';
import { ExceptionDialogComponent } from './exception-dialog/exception-dialog.component';
import { PushNotificationService } from './push-notification.service';

@Injectable()
export class ExceptionService implements Exception {
  buttonLabel: string = 'Got it!';
  message: string;
  status: number;
  errorMessage:string;
  statusText: string;

  constructor(private dialog: MdDialog, private router: Router, private auth: AuthService, private pushNotification: PushNotificationService) { }

  takeAction(error: HttpErrorResponse) : void {
    this.status = error.status;
    this.statusText = error.statusText;
    this.errorMessage = error.message;
    if(error.status === 401) this.unauthenticated(error);
    else if(error.status === 403) this.unauthorized(error);
    else if(error.status === 422) this.unprocessable(error);
    else if(error.status === 500) this.internalServerError(error);
    else this.defaultError(error);
  }
  
  defaultError(error: HttpErrorResponse) {
    this.message = error.message;
    this.openDialog();
  }

  unauthenticated(error: HttpErrorResponse) {
    this.message = 'Please login to continue.';
    this.openDialog().afterClosed().subscribe(result => this.auth.logout());
  }

  unauthorized(error: HttpErrorResponse) {
    this.message = 'This action is unauthorized.';
    this.openDialog();
  }

  unprocessable(error: HttpErrorResponse) {
    this.message = 'Please check the form for errors.';
    this.openDialog();
  }

  internalServerError(error: HttpErrorResponse) { 
    this.message = 'Oops! Something went wrong. Please try again or refresh the page.';
    this.openDialog().afterClosed().subscribe(resp => {
      if(resp) {
        this.pushNotification.simple('Report sent');
        return this.takeAction(resp.error);
      }
    });
  }

  openDialog() : MdDialogRef<ExceptionDialogComponent> {
    return this.dialog.open(ExceptionDialogComponent, {
      data: {
        status: this.status,
        statusText: this.statusText,
        message: this.message,
        errorMessage: this.errorMessage,
        buttonLabel: this.buttonLabel,
      }
    });
  }
}
