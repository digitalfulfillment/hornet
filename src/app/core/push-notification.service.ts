import { Injectable } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { environment } from '../../environments/environment';

@Injectable()
export class PushNotificationService {

  constructor(private snackBar: MdSnackBar) { }

  simple(message: string): void {
    this.snackBar.open(message, 'Okay', {
      duration: 3000,
    });
  }

  browser(data: {body:string, icon?:string}) {
    Notification.requestPermission().then(result => this.notify(data.body, data.icon));
  }

  notify(body:string, icon?:string) {
    new Notification(environment.name, { body, icon: icon ? icon : '/assets/img/2Color-Favicon_512x512-1-raw.png' });
  }
}
