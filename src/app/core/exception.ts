export interface Exception {
    buttonLabel: string;
    message: string,
    statusText: string;
}
