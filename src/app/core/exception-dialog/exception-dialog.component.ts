import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';

import { ApiService } from '../api.service';

@Component({
  selector: 'app-exception-dialog',
  templateUrl: './exception-dialog.component.html',
  styleUrls: ['./exception-dialog.component.scss']
})
export class ExceptionDialogComponent {
  busy: boolean;

  constructor(
    @Inject(MD_DIALOG_DATA) public data: any, 
    private api: ApiService,
    public dialogRef: MdDialogRef<ExceptionDialogComponent>,
  ) { }

  close() {
    this.dialogRef.close();
  }

  payload() {
    return {
      message: this.data.errorMessage,
      status: this.data.status,
      statusText: this.data.statusText,
    }
  }

  sendReport() {
    this.busy = true;

    this.api.post('exception-report', this.payload()).subscribe(
      () => this.busy = false,
      error => {
        this.busy = false;
        this.dialogRef.close({ error });
      },
      () => this.dialogRef.close()
    );
  }
}
