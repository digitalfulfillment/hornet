import { Injectable } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import { AlertDialog } from '../../interfaces/alert-dialog';
import { AlertDialogComponent } from './alert-dialog.component';

@Injectable()
export class AlertDialogService {

  constructor(private dialog: MdDialog) { }

  show(data: AlertDialog) : MdDialogRef<AlertDialogComponent> {
    return this.dialog.open(AlertDialogComponent, { data });
  }
}
