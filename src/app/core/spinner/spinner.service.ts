import { Injectable } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import { SpinnerComponent } from './spinner.component';

@Injectable()
export class SpinnerService {
  spinner: MdDialogRef<SpinnerComponent>;

  constructor(
    public dialog: MdDialog,
  ) { }

  show(message?: string): void {
    this.spinner = this.dialog.open(SpinnerComponent, {
      data: {
        message: message ? message : 'Loading'
      },
      disableClose: true
    });
  }

  hide(): void {
    return this.spinner.close();
  }
}
