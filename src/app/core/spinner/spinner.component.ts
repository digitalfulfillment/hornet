import { Component, Inject, OnInit } from '@angular/core';
import { MD_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  message: string;

  constructor(@Inject(MD_DIALOG_DATA) public data: any) { 
    this.message = this.data.message;
  }

  ngOnInit() {
  }

}
