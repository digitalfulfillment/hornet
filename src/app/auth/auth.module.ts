import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule, MdCardModule, MdDialogModule, MdInputModule, MdProgressSpinnerModule } from '@angular/material';

import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { LoginFormService } from './login-form.service';
import { SettingsGuard } from './roles/settings.guard';
import { QuestionnaireGuard } from './roles/questionnaire.guard';
import { EvaluationGuard } from './roles/evaluation.guard';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserGuard } from './roles/user.guard';

@NgModule({
  imports: [
    ReactiveFormsModule,
    HttpClientModule,
    MdButtonModule,
    MdCardModule,
    MdDialogModule,
    MdInputModule, 
    MdProgressSpinnerModule,
    SharedModule,
    AuthRoutingModule,
  ],
  declarations: [AuthComponent, LoginComponent, ChangePasswordComponent],
  entryComponents:[ChangePasswordComponent],
  providers: [LoginFormService, SettingsGuard, QuestionnaireGuard, EvaluationGuard, UserGuard]
})
export class AuthModule { }
