import { Component, OnInit } from '@angular/core';

import { LoginFormService } from '../login-form.service';
import { AuthService } from '../auth.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error;
  busy: boolean = false;
  passwordResetLink: string;

  constructor(public loginForm: LoginFormService, private authService: AuthService) { }

  ngOnInit()
  {
    this.passwordResetLink = `${environment.apiUrl}/password/reset`;
  }

  login() : void {
    const email = this.loginForm.formGroup.get('email').value;
    const password = this.loginForm.formGroup.get('password').value;

    if(this.loginForm.formGroup.valid && !this.busy) {
      this.busy = true;

      this.authService
        .login(email, password)
        .then(resp => { 
          this.busy = false;
          this.authService.intendedRoute();
        })
        .catch(error => {
          this.busy = false;
          this.error = JSON.parse(error);
        })
    }
  }
}
