import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { ApiService } from '../../core/api.service';
import { AlertDialogService } from '../../core/alert-dialog/alert-dialog.service';
import { AuthService } from '../auth.service';
import { ChangePasswordService } from './change-password.service';
import { PushNotificationService } from '../../core/push-notification.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers: [ChangePasswordService]
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  busy: boolean;
  confirmPasswordSubscription: Subscription;
  invalidPassword: boolean;
  newPasswordSubscription: Subscription;
  oldPasswordSubscription: Subscription;
  passwordsMatch: boolean;
  error: string;

  constructor(
    private alert: AlertDialogService,
    private api: ApiService,
    private auth: AuthService,
    public form: ChangePasswordService,
    private dialogRef: MdDialogRef<ChangePasswordComponent>,
  ) { }

  ngOnInit() {
    this.oldPasswordSubscription = this.form.formGroup.controls['oldPassword'].valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.checkOldPassword(term).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => {
        if(!resp) return this.invalidPassword = true;
        return this.invalidPassword = false;
      });

    this.newPasswordSubscription = this.form.formGroup.controls['newPassword'].valueChanges
      .subscribe(newPassword => {
        if(newPassword == this.form.formGroup.get('confirmPassword').value) return this.passwordsMatch = true;
        return this.passwordsMatch = false;
      });

    this.confirmPasswordSubscription = this.form.formGroup.controls['confirmPassword'].valueChanges
      .subscribe(confirmPassword => {
        if(confirmPassword == this.form.formGroup.get('newPassword').value) return this.passwordsMatch = true;
        return this.passwordsMatch = false;
      });
  }

  ngOnDestroy() {
    this.oldPasswordSubscription.unsubscribe();
    this.newPasswordSubscription.unsubscribe();
    this.confirmPasswordSubscription.unsubscribe();
  }

  checkOldPassword(password) {
    return this.api.post('user/check-password', { password });
  }

  submit() {
    if(this.form.formGroup.valid && this.passwordsMatch && !this.invalidPassword) {
      this.busy = true;

      let user = {
        'old_password': this.form.formGroup.get('oldPassword').value,
        'password': this.form.formGroup.get('newPassword').value,
        'password_confirmation': this.form.formGroup.get('confirmPassword').value,
      }

      this.api.post('user/change-password', user).subscribe(
        resp => {
          this.auth.login(this.auth.user.email, user.password);
          this.busy = false;
        },
        (error: HttpErrorResponse) => { 
          this.dialogRef.close({ error });
          this.busy = false;
        },
        () => {
          this.dialogRef.close(true);
        }
      )
    }
  }
}
