import { Injectable } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { FormValidationMessage } from '../../core/form-validation-message';
import { FormValidationMessageService } from '../../core/form-validation-message.service';

@Injectable()
export class ChangePasswordService implements FormValidationMessage {
  formGroup: FormGroup;

  errors: { oldPassword, newPassword, confirmPassword } = {
    'oldPassword': '',
    'newPassword': '',
    'confirmPassword': '',
  };

  rules: { oldPassword, newPassword, confirmPassword } = {
    'oldPassword' : ['', [
        Validators.required,
        Validators.minLength(8),
      ]
    ],
    'newPassword' : ['', [
        Validators.required,
        Validators.minLength(8),
      ]
    ],
    'confirmPassword' : ['', [
        Validators.required,
        Validators.minLength(8),
      ]
    ],
  }

  validationMessages: { oldPassword, newPassword, confirmPassword } = {
    'oldPassword': {
      'required': 'Old password is required.',
      'minlength': 'Password must be at least 8 characters long.'
    },
    'newPassword': {
      'required': 'New password is required.',
      'minlength': 'Password must be at least 8 characters long.'
    },
    'confirmPassword': {
      'required': 'Confirm password is required.',
      'minlength': 'Password must be at least 8 characters long.'
    },
  }

  constructor(private form: FormValidationMessageService) { 
    form.rules = this.rules;
    form.errors = this.errors;
    form.validationMessages = this.validationMessages;

    this.form.buildForm();
    this.formGroup = this.form.formGroup;
  }

}
