import { TestBed, async, inject } from '@angular/core/testing';

import { EvaluationGuard } from './evaluation.guard';

describe('EvaluationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvaluationGuard]
    });
  });

  it('should ...', inject([EvaluationGuard], (guard: EvaluationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
