import { TestBed, async, inject } from '@angular/core/testing';

import { QuestionnairesGuard } from './questionnaires.guard';

describe('QuestionnairesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionnairesGuard]
    });
  });

  it('should ...', inject([QuestionnairesGuard], (guard: QuestionnairesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
