import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';

import { AuthService } from '../auth.service';

@Injectable()
export class UserGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      return this.checkAccess();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  canLoad() {
    return this.checkAccess();
  }

  checkAccess() : boolean {
    if(this.authService.user.is_admin || this.authService.user.roles.filter(role => role.name == 'manage-users').length)
    {
      return true;
    }
    
    // Navigate to the page not found
    this.router.navigate(['/page-not-found']);
    return false;
  }
}
