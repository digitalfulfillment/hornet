import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AlertDialogService } from '../../core/alert-dialog/alert-dialog.service';
import { Category } from '../../models/category';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { Questionnaire } from '../../models/questionnaire';
import { QuestionnaireForm } from '../questionnaire-form';
import { QuestionnaireDataService } from '../questionnaire-data.service';
import { QuestionnaireNameDialogComponent } from '../questionnaire-name-dialog/questionnaire-name-dialog.component';
import { SpinnerService } from '../../core/spinner/spinner.service';

@Component({
  selector: 'app-edit-questionnaire',
  templateUrl: './edit-questionnaire.component.html',
  styleUrls: ['./edit-questionnaire.component.scss']
})
export class EditQuestionnaireComponent extends QuestionnaireForm implements OnInit {
  formGroup;
  questionnaire: Questionnaire;

  constructor(
    private alertDialog: AlertDialogService,
    private activatedRoute: ActivatedRoute,
    private dataService: QuestionnaireDataService,
    private dialog: MdDialog,
    private exception: ExceptionService,
    private formBuilder: FormBuilder,
    private pushNotification: PushNotificationService,
    private router: Router,
    private spinner: SpinnerService,
  ) { 
    super(alertDialog, formBuilder, dialog);
  }

  ngOnInit() {
    this.formGroup = this.fb.group({
      content: this.fb.array([])
    });

    this.activatedRoute.data
      .subscribe((data: { questionnaire: Questionnaire}) => { 
        this.questionnaire = data.questionnaire
        this.setForm(this.questionnaire);
        this.formGroup.get('content').patchValue(this.questionnaire.content);
      });
  }

  initQuestionnaire(name: string): Questionnaire {
    return new Questionnaire({
      name,
      content: this.formGroup.controls.content.value,
    });
  }

  prepareQuestionnaire(): Questionnaire {
    return new Questionnaire({
      id: this.questionnaire.id,
      name: this.questionnaire.name,
      content: this.formGroup.controls.content.value,
    });
  }

  setForm(questionnaire: Questionnaire) {
    questionnaire.content.forEach((category: Category, categoryIndex: number) => {
      this.addCategory();
      this.setFormQuestions(category, categoryIndex);
    });
  }

  setFormQuestions(category: Category, categoryIndex: number) {
    category.questions.forEach((question, questionIndex) => {
      if(questionIndex < category.questions.length-1) this.addQuestion(categoryIndex);
    });
  }

  saveAs() {
    if(this.formGroup.valid) {
      return this.dialog.open(QuestionnaireNameDialogComponent).afterClosed().subscribe((resp: any) => {
        if(resp) {

          if(resp.error) return this.exception.takeAction(resp.error);

          this.spinner.show('Saving');
          this.store(this.initQuestionnaire(resp)).subscribe(
            () => { 
              this.pushNotification.simple('Questionnaire created.');
              this.spinner.hide();
              this.submitted = true;
            },
            error => {
              this.spinner.hide();
              this.exception.takeAction(error);
            },
            () => this.router.navigate(['/questionnaires'])
          )
        } 
      });
    }

    this.alertForErrors();
  }

  save() {
    if(this.formGroup.valid) {
      this.spinner.show('Saving');
      return this.update(this.prepareQuestionnaire()).subscribe(
        () => {
          this.pushNotification.simple('Changes saved.');
          this.submitted = true;
        },
        error => {
          this.spinner.hide();
          this.exception.takeAction(error);
        },
        () => {
          this.spinner.hide();
          this.router.navigate(['/questionnaires'])
        }
      );
    }

    this.alertForErrors();
  }

  store(questionnaire : Questionnaire): Observable<void> {
    return this.dataService.store(questionnaire);
  }

  update(questionnaire : Questionnaire): Observable<void> {
    return this.dataService.update(questionnaire);
  }
}
