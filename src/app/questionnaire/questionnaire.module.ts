import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule, MdCardModule, MdDialogModule, MdExpansionModule, MdIconModule, MdInputModule, MdListModule, MdMenuModule, MdPaginatorModule, MdProgressSpinnerModule, MdToolbarModule, MdTooltipModule } from '@angular/material';

import { QuestionnaireRoutingModule } from './questionnaire-routing.module';
import { QuestionnaireItemComponent } from './questionnaire-item/questionnaire-item.component';
import { QuestionnaireListComponent } from './questionnaire-list/questionnaire-list.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { SharedModule } from '../shared/shared.module';
import { CreateQuestionnaireComponent } from './create-questionnaire/create-questionnaire.component';
import { QuestionnaireNameDialogComponent } from './questionnaire-name-dialog/questionnaire-name-dialog.component';
import { EditQuestionnaireComponent } from './edit-questionnaire/edit-questionnaire.component';

@NgModule({
  imports: [
    FormsModule,
    MdButtonModule,
    MdCardModule,
    MdDialogModule,
    MdExpansionModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdPaginatorModule,
    MdProgressSpinnerModule,
    MdToolbarModule,
    MdTooltipModule,
    SharedModule,
    QuestionnaireRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    QuestionnaireItemComponent, 
    QuestionnaireListComponent, 
    QuestionnaireComponent, 
    CreateQuestionnaireComponent, 
    QuestionnaireNameDialogComponent, 
    EditQuestionnaireComponent
  ],
  entryComponents: [QuestionnaireNameDialogComponent],
})
export class QuestionnaireModule { }
