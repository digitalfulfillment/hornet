import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { PageEvent } from '@angular/material'; 
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { ExceptionService } from '../../core/exception.service';
import { Paginated } from '../../interfaces/paginated';
import { Questionnaire } from '../../models/questionnaire';
import { QuestionnaireDataService } from '../questionnaire-data.service';

@Component({
  selector: 'app-questionnaire-list',
  templateUrl: './questionnaire-list.component.html',
  styleUrls: ['./questionnaire-list.component.scss']
})
export class QuestionnaireListComponent implements OnInit {
  loading: boolean;
  perPage: number;
  questionnaires: Questionnaire[];
  searchField: FormControl;
  total: number;
  searchFieldSubscription: Subscription;

  constructor(
    private dataService: QuestionnaireDataService, 
    private dialog: MdDialog,
    private exception: ExceptionService,
  ) { }

  ngOnInit() {
    this.searchField = new FormControl();
    this.searchFieldSubscription = this.searchField.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => {
        this.loading = true;
        return this.searchUserInput(term).catch(error => {
          this.loading = false;
          this.exception.takeAction(error);
          return Observable.empty();
        });
      })
      .subscribe(
        resp => {
          this.loading = false;
          this.setData(resp);
        },
      );

    this.loading = true;
    this.initData();
  }

  ngOnDestroy() {
    this.searchFieldSubscription.unsubscribe();
  }

  initData(): void {
    this.dataService.paginate().subscribe(
      data => this.setData(data),
      error => {
        this.loading = false;
        this.exception.takeAction(error);
      },
      () => this.loading = false
    );
  }

  setData(data) {
    this.total = data.total;
    this.perPage = data.per_page;
    this.questionnaires = data.data;
  }

  pageEventCall(pageEvent: PageEvent) {
    this.loading = true;

    if(this.searchField.value)
    {
      return this.searchUserInput(this.searchField.value, pageEvent.pageIndex + 1)
        .subscribe(
          data => this.setData(data),
          error => {
            this.loading = false;
            this.exception.takeAction(error)
          },
          () => this.loading = false
        );
    }

    this.dataService.paginate(pageEvent.pageIndex + 1)
      .subscribe(
        data => this.setData(data),
        error => {
          this.loading = false;
          this.exception.takeAction(error)
        },
        () => this.loading = false
      );
  }

  searchUserInput(term: string, page?: number): Observable<Paginated> {
    let query = {
      name: term
    };

    return this.dataService.search(query, page);
  }
}
