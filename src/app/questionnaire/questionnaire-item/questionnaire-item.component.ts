import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { Observable } from 'rxjs';

import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { Questionnaire } from '../../models/questionnaire';
import { QuestionnaireDataService } from '../questionnaire-data.service';
import { SpinnerService } from '../../core/spinner/spinner.service';

@Component({
  selector: 'app-questionnaire-item',
  templateUrl: './questionnaire-item.component.html',
  styleUrls: ['./questionnaire-item.component.scss']
})
export class QuestionnaireItemComponent implements OnInit {
  questionnaire: Questionnaire;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private dataService: QuestionnaireDataService,
    private dialog: MdDialog, 
    private exception: ExceptionService,
    private pushNotification: PushNotificationService,
    private router: Router,
    private spinner: SpinnerService,
  ) { }
  
  ngOnInit() {
    this.activatedRoute.data
      .subscribe((data: { questionnaire: Questionnaire}) => this.questionnaire = data.questionnaire);
  }

  confirmDelete() {
    this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: `Delete Questionnaire`,
        message: 'Are you sure you want to delete this questionnaire?',
        confirm: 'Delete',
        cancel: 'Cancel',
      }
    }).afterClosed().subscribe(resp => {
      if(resp) {
        this.spinner.show();
        this.delete();
      };
    });
  }

  delete() {
    this.dataService.delete(this.questionnaire).subscribe(
      () => this.pushNotification.simple('Questionnaire delete.'),
      error => {
        this.spinner.hide();
        this.exception.takeAction(error);
      },
      () => {
        this.spinner.hide();
        this.router.navigate(['/questionnaires']);
      }
    )
  }

  edit() {
    this.router.navigate([`/questionnaires/${this.questionnaire.id}/edit`]);
  }
}
