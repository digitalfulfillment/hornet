import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AlertDialogService } from '../../core/alert-dialog/alert-dialog.service';
import { ExceptionService } from '../../core/exception.service';
import { PushNotificationService } from '../../core/push-notification.service';
import { Questionnaire } from '../../models/questionnaire';
import { QuestionnaireForm } from '../questionnaire-form';
import { QuestionnaireDataService } from '../questionnaire-data.service';
import { QuestionnaireNameDialogComponent } from '../questionnaire-name-dialog/questionnaire-name-dialog.component';
import { SpinnerService } from '../../core/spinner/spinner.service';

@Component({
  selector: 'app-create-questionnaire',
  templateUrl: './create-questionnaire.component.html',
  styleUrls: ['./create-questionnaire.component.scss']
})
export class CreateQuestionnaireComponent extends QuestionnaireForm implements OnInit {
  questionnaire: Questionnaire;

  constructor(
    private alertDialog: AlertDialogService,
    private exception: ExceptionService,
    private formBuilder: FormBuilder,
    private dataService: QuestionnaireDataService,
    private dialog: MdDialog,
    private pushNotification: PushNotificationService,
    private router: Router,
    private spinner: SpinnerService,
  ) {
    super(alertDialog, formBuilder, dialog);
  }

  ngOnInit() {
    this.formGroup = this.fb.group({
      content: this.fb.array([
        this.initCategory(),
      ])
    });
  }

  initQuestionnaire(name: string) {
    this.questionnaire = new Questionnaire({
      name,
      content: this.formGroup.controls.content.value,
    });
  }

  storeQuestionaire() {
    this.dataService.store(this.questionnaire).subscribe(
      () => { 
        this.pushNotification.simple('Questionnaire created!');
        this.spinner.hide();
        this.submitted = true;
      },
      error => { 
        this.exception.takeAction(error)
        this.spinner.hide();
      },
      () => this.router.navigate(['/questionnaires'])
    );
  }

  submit() {
    if(this.formGroup.valid) {
      return this.dialog.open(QuestionnaireNameDialogComponent).afterClosed().subscribe((resp: any) => {
        if(resp) {

          if(resp.error) return this.exception.takeAction(resp.error);
          
          this.spinner.show('Saving');
          this.initQuestionnaire(resp);       
          this.storeQuestionaire();
        } 
      });
    }

    this.alertForErrors();
  }
}
