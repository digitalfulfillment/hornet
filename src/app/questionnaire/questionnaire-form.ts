import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { AlertDialogService } from '../core/alert-dialog/alert-dialog.service';
import { ConfirmationDialog } from '../interfaces/confirmation-dialog';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';

export class QuestionnaireForm {
    formGroup;
    submitted: boolean;

    constructor(
        protected a: AlertDialogService,
        protected fb: FormBuilder,
        protected d: MdDialog,
    ) { }

    alertForErrors() {
        const dialog = {
            title: 'Heads up!',
            message: 'Please check form for errors.',
            okay: 'Got it!'
        }

        this.a.show(dialog);
    }

    canDeactivate() : Observable<boolean> | boolean {
        return this.confirmDiscardChanges();
    }
    
    confirmDiscardChanges() : Observable<boolean> | boolean {
        if(this.formGroup.dirty && !this.submitted) {
          return this.d.open(ConfirmationDialogComponent, {
            data: {
              title: 'Discard changes',
              message: 'Do you want to discard changes?',
              confirm: 'Discard',
              cancel: 'Cancel'
            }
          })
          .afterClosed()
          .map((resp: boolean) => {
            return resp;
          });
        }
    
        return true;
    }

    confirmRemove(data: ConfirmationDialog) : MdDialogRef<ConfirmationDialogComponent> {
        return this.d.open(ConfirmationDialogComponent, { data });
    }

    initCategory(): FormGroup {
        return this.fb.group({
            'name': ['', [Validators.required]],
            'questions': this.fb.array([
                this.initQuestion(),
            ])
        });
    }
    
    initQuestion(): FormGroup {
        return this.fb.group({
          'question': ['', Validators.required],
          'points': ['', [
            Validators.required,
            Validators.pattern('^[0-9]+$')
          ]]
        });
    }
    
    addCategory() {
        const control = <FormArray>this.formGroup.controls['content'];
        control.push(this.initCategory());
    }
      
    removeCategory(i: number) {
        let dialog = {
            title: 'Remove Category',
            message: 'Do you want to remove this category?',
            confirm: 'Remove',
            cancel: 'Cancel'
        }

        this.confirmRemove(dialog)
            .afterClosed()
            .subscribe(resp => {
                if(resp) {
                    const control = <FormArray>this.formGroup.controls['content'];
                    control.removeAt(i);
                }
            })
    }
    
    addQuestion(i: number) {
        const control = this.formGroup.controls.content.controls[i].controls.questions;
        control.push(this.initQuestion());
    }
    
    removeQuestion(categoryIdx: number, questionIdx: number) {
        let dialog = {
            title: 'Remove Question',
            message: 'Do you want to remove this question?',
            confirm: 'Remove',
            cancel: 'Cancel'
        }

        this.confirmRemove(dialog)
        .afterClosed()
        .subscribe(resp => {
            const control = this.formGroup.controls.content.controls[categoryIdx].controls.questions;
            control.removeAt(questionIdx);
        })
    }
}
