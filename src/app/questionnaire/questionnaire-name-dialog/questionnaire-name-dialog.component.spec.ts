import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireNameDialogComponent } from './questionnaire-name-dialog.component';

describe('QuestionnaireNameDialogComponent', () => {
  let component: QuestionnaireNameDialogComponent;
  let fixture: ComponentFixture<QuestionnaireNameDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireNameDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireNameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
