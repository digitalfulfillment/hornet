import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';

import { Questionnaire } from '../../models/questionnaire';
import { QuestionnaireDataService } from '../questionnaire-data.service';
import { QuestionnaireNameFormService } from './questionnaire-name-form.service';
import { Paginated } from '../../interfaces/paginated';

@Component({
  selector: 'app-questionnaire-name-dialog',
  templateUrl: './questionnaire-name-dialog.component.html',
  styleUrls: ['./questionnaire-name-dialog.component.scss'],
  providers: [QuestionnaireNameFormService]
})
export class QuestionnaireNameDialogComponent implements OnInit {
  hasDuplicate: boolean;

  constructor(
    private dataService: QuestionnaireDataService,
    public form: QuestionnaireNameFormService,
    public dialogRef: MdDialogRef<QuestionnaireNameDialogComponent>,
  ) { }

  ngOnInit() {
    this.form.formGroup.get('name').valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(name =>  this.checkDuplicate(name).catch(error => {
        this.dialogRef.close({ error });
        return Observable.empty();
      }))
      .subscribe(resp => this.hasDuplicate = resp ? true : false);    
  }

  checkDuplicate(name: string) : Observable<Paginated> {
    let agent = new Questionnaire({
      name: name
    });

    return this.dataService.checkDuplicate(agent);
  }

  submit() {
    if(!this.hasDuplicate && this.form.formGroup.valid) {
      this.dialogRef.close(this.form.formGroup.get('name').value);
    }
  }
}
