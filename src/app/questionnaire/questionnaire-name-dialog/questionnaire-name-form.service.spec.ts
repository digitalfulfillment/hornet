import { TestBed, inject } from '@angular/core/testing';

import { QuestionnaireNameFormService } from './questionnaire-name-form.service';

describe('QuestionnaireNameFormService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionnaireNameFormService]
    });
  });

  it('should be created', inject([QuestionnaireNameFormService], (service: QuestionnaireNameFormService) => {
    expect(service).toBeTruthy();
  }));
});
