import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '../core/api.service';
import { Paginated } from '../interfaces/paginated';
import { Questionnaire } from '../models/questionnaire';

const url = 'questionnaire';

@Injectable()
export class QuestionnaireDataService {

  constructor(private api: ApiService) { }

  checkDuplicate(questionnaire: Questionnaire) {
    return this.api.post(`${url}/check-duplicate`, questionnaire);
  }

  delete(questionnaire: Questionnaire) {
    return this.api.delete(`${url}/${questionnaire.id}`);
  }

  paginate(page?: number): Observable<Paginated> {
    return this.api.get(`${url}?page=${page}`);
  }

  search(query: object, page?: number): Observable<Paginated> {
    return this.api.post(`${url}/search?page=${page}`, query);
  }

  show(id: number): Observable<Questionnaire> {
    return this.api.get(`${url}/${id}`);
  }

  store(questionnaire: Questionnaire) : Observable<void> {
    return this.api.post(`${url}`, questionnaire);
  }

  update(questionnaire: Questionnaire) : Observable<void> {
    return this.api.put(`${url}/${questionnaire.id}`, questionnaire);
  }
}
