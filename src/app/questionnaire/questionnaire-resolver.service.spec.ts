import { TestBed, inject } from '@angular/core/testing';

import { QuestionnaireResolverService } from './questionnaire-resolver.service';

describe('QuestionnaireResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionnaireResolverService]
    });
  });

  it('should be created', inject([QuestionnaireResolverService], (service: QuestionnaireResolverService) => {
    expect(service).toBeTruthy();
  }));
});
