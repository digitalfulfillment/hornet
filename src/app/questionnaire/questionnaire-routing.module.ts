import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanDeactivateGuard } from '../core/can-deactivate.guard';
import { CreateQuestionnaireComponent } from './create-questionnaire/create-questionnaire.component';
import { EditQuestionnaireComponent } from './edit-questionnaire/edit-questionnaire.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { QuestionnaireResolverService } from './questionnaire-resolver.service';
import { QuestionnaireItemComponent } from './questionnaire-item/questionnaire-item.component';
import { QuestionnaireListComponent } from './questionnaire-list/questionnaire-list.component';

const routes: Routes = [
  { 
    path: '',
    component:QuestionnaireComponent,
    children: [
      {
        path: '',
        component: QuestionnaireListComponent
      },
      {
        path: 'create',
        component: CreateQuestionnaireComponent,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: ':questionnaireId',
        component: QuestionnaireItemComponent,
        resolve: {
          questionnaire: QuestionnaireResolverService
        },
      },
      {
        path: ':questionnaireId/edit',
        component: EditQuestionnaireComponent,
        resolve: {
          questionnaire: QuestionnaireResolverService
        },
        canDeactivate: [CanDeactivateGuard]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionnaireRoutingModule { }
