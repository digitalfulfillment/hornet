import { Injectable } from '@angular/core';
import Pusher from 'pusher-js';
import { Subject } from 'rxjs/Subject';

import { environment } from '../environments/environment';
import { AuthService } from './auth/auth.service';
import { PushNotificationService } from './core/push-notification.service';

@Injectable()
export class PusherService {
  pusher: Pusher;

  private newNotification = new Subject<any>();

  newNotification$ = this.newNotification.asObservable();
  
  constructor(private auth: AuthService, private pushNotification: PushNotificationService) { 
    Pusher.logToConsole = environment.production ? false : true;

    this.pusher = new Pusher(environment.pusher.key, {
      cluster: environment.pusher.cluster,
      encrypted: true
    });
  }

  init() {
    this.pusher.subscribe('web')
      .bind('evaluation.created', data => {
        if(this.auth.user.id != data.user.id) {
          this.notify();
          this.pushNotification.browser({ body: `${data.user.name} has created a new evaluation.`});
        } 
      })
      .bind('evaluation.updated', data => {
        if(this.auth.user.id != data.user.id) {
          this.notify();
          this.pushNotification.browser({ body: `${data.user.name} has updated an evaluation.` });
        }
      });
  }

  disconnect() {
    this.pusher.disconnect();
  }

  notify() {
    this.newNotification.next();
  }
}