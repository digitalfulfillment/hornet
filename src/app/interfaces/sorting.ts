export interface Sorting {
    sortType: string;
    sortReverse: boolean;
    sortHeaders: [{ label, value}];
    sortedBy: string;
    sort(data: any[], key?: string, order?: boolean): any;
}
