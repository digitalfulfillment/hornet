export interface AlertDialog {
    title: string;
    message: string;
    okay: string;
}
