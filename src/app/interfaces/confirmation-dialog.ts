export interface ConfirmationDialog {
    title: string;
    message: string;
    confirm: string;
    cancel: string;
}
