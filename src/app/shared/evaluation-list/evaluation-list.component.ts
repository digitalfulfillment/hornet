import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Evaluation } from '../../models/evaluation';
import { EvaluationSortingService } from '../../evaluation/evaluation-sorting.service';
import { EvaluationListService } from './evaluation-list.service';

@Component({
  selector: 'app-evaluation-list',
  templateUrl: './evaluation-list.component.html',
  styleUrls: ['./evaluation-list.component.scss'],
})
export class EvaluationListComponent implements OnInit, OnDestroy {
  @Input('evaluations') evaluations: Evaluation[];
  @Input('loading') loading: boolean;

  sortingSubscription: Subscription;

  constructor(public sorting: EvaluationSortingService, private evaluationList:EvaluationListService) {  }

  ngOnInit() {
    this.sortingSubscription = this.sorting.sortCall$.subscribe(() => this.evaluations = this.sorting.sort(this.evaluations));
  }

  ngOnDestroy() {
    this.sortingSubscription.unsubscribe();
  }

  view(evaluation: Evaluation) {
    this.evaluationList.view(evaluation);
  }
}
