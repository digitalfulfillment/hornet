import { TestBed, inject } from '@angular/core/testing';

import { EvaluationListService } from './evaluation-list.service';

describe('EvaluationListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvaluationListService]
    });
  });

  it('should be created', inject([EvaluationListService], (service: EvaluationListService) => {
    expect(service).toBeTruthy();
  }));
});
