import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { Evaluation } from '../../models/evaluation';

@Injectable()
export class EvaluationListService {
  private evaluation: Subject<Evaluation> = new Subject();
  
  evaluation$ = this.evaluation.asObservable();

  view(evaluation: Evaluation) {
    return this.evaluation.next(evaluation);
  }
}
