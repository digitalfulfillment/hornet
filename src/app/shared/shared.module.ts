import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MdButtonModule, MdDialogModule, MdTooltipModule, MdProgressSpinnerModule } from '@angular/material';
import { MomentModule } from 'angular2-moment';
import { RouterModule } from '@angular/router';

import { ErrorComponent } from './error/error.component';
import { InputErrorComponent } from './input-error/input-error.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { EvaluationListComponent } from './evaluation-list/evaluation-list.component';
import { EvaluationListService } from './evaluation-list/evaluation-list.service';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MdButtonModule,
    MdDialogModule,
    MdProgressSpinnerModule,
    MdTooltipModule,
    MomentModule,
    RouterModule
  ],
  exports: [
    CommonModule,
    FlexLayoutModule,
    MomentModule,
    ErrorComponent,
    InputErrorComponent,
    ConfirmationDialogComponent,
    EvaluationListComponent,
  ],
  declarations: [ErrorComponent, InputErrorComponent, ConfirmationDialogComponent, EvaluationListComponent],
  entryComponents: [ConfirmationDialogComponent],
  providers:[EvaluationListService],
})
export class SharedModule { }
